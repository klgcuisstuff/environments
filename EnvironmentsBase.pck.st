'From Cuis 5.0 [latest update: #4438] on 4 November 2020 at 4:39:52 pm'!
'Description I provide super classes to implement environments aka. namespaces.'!
!provides: 'EnvironmentsBase' 1 21!
!requires: 'Cuis-Base' 50 4438 nil!
!requires: 'EnvironmentAwareness' 1 13 nil!
SystemOrganization addCategory: 'EnvironmentsBase'!


!classDefinition: #EnvironmentAwareSystemOrganizer category: 'EnvironmentsBase'!
SystemOrganizer subclass: #EnvironmentAwareSystemOrganizer
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentsBase'!
!classDefinition: 'EnvironmentAwareSystemOrganizer class' category: 'EnvironmentsBase'!
EnvironmentAwareSystemOrganizer class
	instanceVariableNames: ''!

!classDefinition: #AbstractEnvironmentClassBuilder category: 'EnvironmentsBase'!
ClassBuilder subclass: #AbstractEnvironmentClassBuilder
	instanceVariableNames: 'environment'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentsBase'!
!classDefinition: 'AbstractEnvironmentClassBuilder class' category: 'EnvironmentsBase'!
AbstractEnvironmentClassBuilder class
	instanceVariableNames: ''!

!classDefinition: #EnvironmentImplementationsDictionary category: 'EnvironmentsBase'!
IdentityDictionary subclass: #EnvironmentImplementationsDictionary
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentsBase'!
!classDefinition: 'EnvironmentImplementationsDictionary class' category: 'EnvironmentsBase'!
EnvironmentImplementationsDictionary class
	instanceVariableNames: ''!

!classDefinition: #AbstractEnvironment category: 'EnvironmentsBase'!
Object subclass: #AbstractEnvironment
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentsBase'!
!classDefinition: 'AbstractEnvironment class' category: 'EnvironmentsBase'!
AbstractEnvironment class
	instanceVariableNames: ''!

!classDefinition: #AbstractEnvironmentManager category: 'EnvironmentsBase'!
Object subclass: #AbstractEnvironmentManager
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentsBase'!
!classDefinition: 'AbstractEnvironmentManager class' category: 'EnvironmentsBase'!
AbstractEnvironmentManager class
	instanceVariableNames: ''!


!EnvironmentAwareSystemOrganizer commentStamp: '<historical>' prior: 0!
I am a system organizer that is aware of environment aware symbols.!

!AbstractEnvironmentClassBuilder commentStamp: '<historical>' prior: 0!
I am the abstract super class for all environment specific class builders.!

!EnvironmentImplementationsDictionary commentStamp: '<historical>' prior: 0!
I am the registry for all the different environment implementations.!

!AbstractEnvironmentManager commentStamp: '<historical>' prior: 0!
I am the abstract super class for all environment managers.

I describe Cuis' core's interface to an environment implementation.!

!AbstractEnvironmentClassBuilder methodsFor: 'accessing' stamp: 'KLG 10/5/2020 16:54:31'!
environment
	"Answer the environment."

	^ environment ! !

!EnvironmentImplementationsDictionary methodsFor: 'shrinking' stamp: 'KLG 10/5/2020 16:04:09'!
removeSelector: descriptor
	"Safely remove a selector from a class (or metaclass). If the class
	or the method doesn't exist anymore, never mind and answer nil.
	This method should be used instead of 'Class removeSelector: #method'
	to omit global class references.
	
	Answer the behaviour handling this remove request,
	otherwise answer nil.
	
	See: See SystemDictionary>>#removeSelector:."
	
	self do: [ :manger |
		manger removeSelector: descriptor :: ifNotNil: [  :affectedBehaviour |
			^ affectedBehaviour ] ].
	^ nil
! !

!AbstractEnvironment methodsFor: 'shrinking' stamp: 'KLG 10/12/2020 10:47:52'!
removeSelector: descriptor
	"Safely remove a selector from a class (or metaclass). If the class
	or the method doesn't exist anymore, never mind and answer nil.
	This method should be used instead of 'Class removeSelector: #method'
	to omit global class references."

	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'compiling' stamp: 'KLG 10/6/2020 15:32:13'!
bindingOf: aVarName
	"Answer the binding of a variable name.
	
	Answer nil if the variable is not bound in this environment."

	^ self subclassResponsibility! !

!AbstractEnvironmentManager methodsFor: 'shrinking' stamp: 'KLG 10/5/2020 16:04:02'!
removeSelector: descriptor
	"Safely remove a selector from a class (or metaclass). If the class
	or the method doesn't exist anymore, never mind and answer nil.
	This method should be used instead of 'Class removeSelector: #method'
	to omit global class references.
	
	Answer the behaviour handling this remove request,
	otherwise answer nil.
	
	See: See SystemDictionary>>#removeSelector:."
	
	^ self subclassResponsibility! !

!EnvironmentAwareSystemOrganizer methodsFor: 'accessing' stamp: 'KLG 10/13/2020 18:19:50'!
environmentAwareNumberOfCategoryOfElement: element 
	"Answer the index of the category with which the argument, element, is 
	associated."

	| categoryIndex elementIndex |
	categoryIndex _ 1.
	elementIndex _ 0.
	[(elementIndex _ elementIndex + 1) <= elementArray size]
		whileTrue: 
			["point to correct category"
			[elementIndex > (categoryStops at: categoryIndex)]
				whileTrue: [categoryIndex _ categoryIndex + 1].
			"see if this is element"
			element == (elementArray at: elementIndex) ifTrue: [^categoryIndex]].
	^0! !

!EnvironmentAwareSystemOrganizer methodsFor: 'accessing' stamp: 'KLG 10/14/2020 09:39:52'!
environmentAwareRemoveElement: element 
	"Remove the selector, element, from all categories."

	| categoryIndex elementIndex nextStop newElements |
	categoryIndex _ 1.
	elementIndex _ 0.
	nextStop _ 0.
	"nextStop keeps track of the stops in the new element array"
	newElements _ WriteStream on: (Array new: elementArray size).
	[(elementIndex _ elementIndex + 1) <= elementArray size]
		whileTrue: 
			[[elementIndex > (categoryStops at: categoryIndex)]
				whileTrue: 
					[categoryStops at: categoryIndex put: nextStop.
					categoryIndex _ categoryIndex + 1].
			(elementArray at: elementIndex) == element
				ifFalse: 
					[nextStop _ nextStop + 1.
					newElements nextPut: (elementArray at: elementIndex)]].
	[categoryIndex <= categoryStops size]
		whileTrue: 
			[categoryStops at: categoryIndex put: nextStop.
			categoryIndex _ categoryIndex + 1].
	elementArray _ newElements contents! !

!EnvironmentAwareSystemOrganizer methodsFor: 'accessing' stamp: 'KLG 10/13/2020 18:20:57'!
numberOfCategoryOfElement: element 
	"Answer the index of the category with which the argument, element, is 
	associated."

	element doWithEnvironmentIfAware: [ :ignoredEnvironment |
		^ self environmentAwareNumberOfCategoryOfElement: element ].
	^ super numberOfCategoryOfElement: element ! !

!EnvironmentAwareSystemOrganizer methodsFor: 'accessing' stamp: 'KLG 10/14/2020 09:41:10'!
removeElement: element 
	"Remove the selector, element, from all categories."

	element doWithEnvironmentIfAware: [ :ignoredEnvironment |
		^ self environmentAwareRemoveElement: element ].
	^ super removeElement: element ! !

!EnvironmentAwareSystemOrganizer methodsFor: 'copying' stamp: 'KLG 10/13/2020 18:28:11'!
copyFromSystemOrganizer: aSystemOrganizer
	"Copy the data from aSystemOrganizer.
	
	This will be done only once, so some dirty means are OK :)"

	categoryArray _ aSystemOrganizer instVarNamed: #categoryArray :: copy.
	categoryStops _ aSystemOrganizer instVarNamed: #categoryStops :: copy.
	elementArray _ aSystemOrganizer instVarNamed: #elementArray :: copy! !

!EnvironmentAwareSystemOrganizer class methodsFor: 'class initialization' stamp: 'KLG 10/21/2020 16:46:21'!
initialize
	"Install me an instance of mine as new SystemOrganization."

	"Not sending super initialize"
	
	| newInstance |
	newInstance _ self new copyFromSystemOrganizer: SystemOrganization.
	SystemOrganization becomeForward: newInstance! !

!AbstractEnvironmentClassBuilder methodsFor: 'public' stamp: 'KLG 10/12/2020 12:20:15'!
superclass: newSuper
	subclass: t instanceVariableNames: f 
	classVariableNames: d poolDictionaries: s category: cat 
	environment: e
	"This is the standard initialization message for creating a new class as a 
	subclass of an existing class in an environment."
	
	| answer className |
	environment _ self environmentFromSpecification: e.
	className _ t forEnvironment: environment.
	answer _ 		super
		superclass: newSuper
		subclass: className instanceVariableNames: f 
		classVariableNames: d poolDictionaries: s category: cat .	
	^ answer! !

!AbstractEnvironmentClassBuilder methodsFor: 'subclass hooks' stamp: 'KLG 10/5/2020 16:54:31'!
configureNewMetaclass: aMetaclass
	"Configure a newly created  metaclass.
	
	The aMetaclass maybe the result of a shallow copy, so take care.
	
	Subclass me if you need special configuration."
	
	aMetaclass configureFromClassBuilder: self! !

!AbstractEnvironmentClassBuilder methodsFor: 'subclass hooks' stamp: 'KLG 10/5/2020 16:59:26'!
environmentFromSpecification: anEnvironmentSpecification
	"Answer the environment denoted by anEnvironmentSpecification.
	
	anEnvironmentSpecification is usallly a symbol or an array of symbols."

	^ self subclassResponsibility! !

!AbstractEnvironmentClassBuilder class methodsFor: 'instance creation' stamp: 'KLG 10/5/2020 18:24:00'!
metaclassClass
	"Answer the proper meta class for me."

	^ self subclassResponsibility! !

!EnvironmentImplementationsDictionary methodsFor: 'class names' stamp: 'KLG 10/3/2020 08:13:27'!
classNamed: className 
	"className is either a class name or a class name followed by ' class'.
	Answer the class or metaclass it names.
	Answer nil if no class by that name.
	
	See SystemDictionary>>#classNamed:"

	self do: [ :manager |
		manager classNamed: className :: ifNotNil: [ :classFound | ^ classFound ] ].
	^ nil! !

!EnvironmentImplementationsDictionary methodsFor: 'class names' stamp: 'KLG 10/4/2020 17:05:32'!
fillClassNameCache: aClassSet andNonClassNameCache: aNonClassSet
	"Ask the implementation to fill the caches.
	
	See: SystemDictionary>>#fillCaches"

	self do: [ :manager |
		manager 
			fillClassNameCache: aClassSet
			andNonClassNameCache: aNonClassSet ]! !

!EnvironmentImplementationsDictionary methodsFor: 'class names' stamp: 'KLG 10/4/2020 17:06:12'!
hasClassNamed: aString
	"Answer whether there is a class of the given name, but don't intern aString if it's not alrady interned.  4/29/96 sw
	
	See SystemDictionary>>#hasClassNamed:
	"

	self do: [ :manager |
		manager hasClassNamed: aString :: ifTrue: [ ^ true ] ].
	^ false.! !

!EnvironmentImplementationsDictionary methodsFor: 'class names' stamp: 'KLG 10/4/2020 17:06:58'!
prepareToRenameClass: aClass as: newName 
	"Rename a class to a new name.
	
	Ask all environment implementations to handle that request.
	Implementations are up to themselves, when it comes
	to manage categories.
	
	See SystemDictionary>>#prepareToRenameClass:as: "

	self do: [ :manger |
		manger prepareToRenameClass: aClass as: newName :: ifTrue: [
			^ true ] ].
	^ false! !

!EnvironmentImplementationsDictionary methodsFor: 'class names' stamp: 'KLG 10/4/2020 17:08:19'!
removeClassNamed: aName
	"Invoked from fileouts:  if there is currently a class in the system named aName, then remove it.  If anything untoward happens, report it in the Transcript.  
	
	Ask the implementations to handle that case.
	
	See SystemDictionary>>#removeClassNamed:"
	
	self flag: #ProbablyOverkill.
	
	self do: [ :manager |
		manager removeClassNamed: aName :: ifTrue: [ ^ true ] ].
	^ false

! !

!EnvironmentImplementationsDictionary methodsFor: 'class names' stamp: 'KLG 10/4/2020 17:09:36'!
renameClassNamed: oldName as: newName
	"Invoked from fileouts:  if there is currently a class in the system named oldName, then rename it to newName.  If anything untoward happens, report it in the Transcript.  
	
	Ask the environment implementations to handle that case.
	
	See SystemDictionary>>#renameClassNamed:as:"

	self do: [ :manger |
		manger renameClassNamed: oldName as: newName :: ifTrue: [ ^ true ] ].
	^ false! !

!EnvironmentImplementationsDictionary methodsFor: 'housekeeping' stamp: 'KLG 10/5/2020 15:57:50'!
addObsoleteClassesTo: anOrderedCollection
	"Ask all implementations to add their obsolete classes to anOrderedColletion.
	
	See SystemDictionary>>#obsoleteClasses"

	self do: [ :manager | manager addObsoleteClassesTo: anOrderedCollection ]! !

!EnvironmentImplementationsDictionary methodsFor: 'housekeeping' stamp: 'KLG 10/4/2020 17:10:55'!
cleanOutUndeclared
	"Clean undeclared stuff in every environment implementation.
	
	See See SystemDictionary>>#cleanOutUndeclared"
	
	self do: [ :manger | manger cleanOutUndeclared ]! !

!EnvironmentImplementationsDictionary methodsFor: 'retrieving' stamp: 'KLG 10/4/2020 17:12:25'!
addToPoolUsers: aDictionary
	"Add references to shared pools to aDictionary.
	
	See SystemDictionary>>#poolUsers"

	self do: [ :manager | manager addToPoolUsers: aDictionary]! !

!EnvironmentImplementationsDictionary methodsFor: 'retrieving' stamp: 'KLG 10/4/2020 17:11:29'!
allBehaviorsDo: aBlock 
	"Evaluate the argument, aBlock, for each kind of Behavior in the system 
	(that is, Object and its subclasses).
	
	See SystemDictionary>>#allBehaviorsDo:"

	self do: [ :manager | manager allBehaviorsDo: aBlock ]! !

!EnvironmentImplementationsDictionary methodsFor: 'scanning' stamp: 'KLG 10/17/2020 16:37:21'!
scanClassDenotation: classDenotation
	"Dispatch the class denotation to each implementation.
	
	If one answers a non nil answer. use it as a scanned class denotation."
	
	self do: [ :manager |
		manager scanClassDenotation: classDenotation :: ifNotNil: [ :denotationFound |
			^ denotationFound ] ].
	^ nil! !

!EnvironmentImplementationsDictionary class methodsFor: 'class initialization' stamp: 'KLG 10/2/2020 21:16:31'!
initialize
	"Initialize me. That means: Create my sole instance."

	super initialize.
	Smalltalk at: #EnvironmentImplementations put: self new! !

!AbstractEnvironment methodsFor: 'class names' stamp: 'KLG 10/12/2020 10:33:47'!
classNamed: className
	"className is either a class name or a class name followed by ' class'.
	Answer the class or metaclass it names.
	Answer nil if no class by that name."
	"
	Smalltalk classNamed: #Point
	Smalltalk classNamed: 'Point'
	Smalltalk classNamed: 'Point class'
	Smalltalk classNamed: 'BogusClassName'
	Smalltalk classNamed: 'BogusClassName class'

	Smalltalk classNamed: #Display
	Smalltalk classNamed: 'Display'
	Smalltalk classNamed: 'Display class'
	"
	
	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'class names' stamp: 'KLG 10/12/2020 10:37:57'!
hasClassNamed: aString
	"Answer whether there is a class of the given name, but don't intern aString if it's not alrady interned.  4/29/96 sw
	Smalltalk hasClassNamed: 'Morph'
	Smalltalk hasClassNamed: 'Display'
	Smalltalk hasClassNamed: 'xMorph'
	"

	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'class names' stamp: 'KLG 10/12/2020 10:40:02'!
prepareToRenameClass: aClass as: newName 

	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'class names' stamp: 'KLG 10/12/2020 10:47:20'!
removeClassNamed: aName
	"Invoked from fileouts:  if there is currently a class in the system named aName, then remove it.  If anything untoward happens, report it in the Transcript.  "

	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'class names' stamp: 'KLG 10/12/2020 10:46:48'!
renameClassNamed: oldName as: newName
	"Invoked from fileouts:  if there is currently a class in the system named oldName, then rename it to newName.  If anything untoward happens, report it in the Transcript.  "

	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'accessing' stamp: 'KLG 11/4/2020 16:18:23'!
associationAt: key ifAbsent: aBlock 
	"Answer the association with the given key.
	If key is not found, return the result of evaluating aBlock."

	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'accessing' stamp: 'KLG 10/12/2020 11:48:32'!
at: aKeySymbol
	"Answer the object at aKeySymbol"

	^ self subclassResponsibility ! !

!AbstractEnvironment methodsFor: 'accessing' stamp: 'KLG 10/12/2020 11:48:56'!
at: aKeySymbol ifAbsent: aBlock
	"Answer the object at aKeySymbol"

	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'accessing' stamp: 'KLG 10/12/2020 11:49:17'!
at: aKeySymbol ifAbsentPut: aBlock
	"Answer the object at aKeySymbol"

	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'accessing' stamp: 'KLG 10/12/2020 11:49:31'!
at: aKey ifPresent: aBlock
	"Execute aBlock if aKey is found."

	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'accessing' stamp: 'KLG 10/13/2020 11:56:36'!
at: key ifPresent: presentBlock ifAbsent: absentBlock
	"Answer the value associated with the key or, if key isn't found,
	answer the result of evaluating aBlock."
	"
		Smalltalk at: #zork ifPresent: [ :cls | (cls name, ' present') print ] ifAbsent: [ 'zork absent' print ]
		Smalltalk at: #Number ifPresent: [ :cls | (cls name, ' present') print ] ifAbsent: [ 'Number absent' print ]
	"
	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'accessing' stamp: 'KLG 10/12/2020 11:49:54'!
at: aKeySymbol put: anObject
	"Put anObject into the environment."

	^ self subclassResponsibility! !

!AbstractEnvironment methodsFor: 'removing' stamp: 'KLG 10/14/2020 09:46:53'!
removeKey: key ifAbsent: aBlock
	"Remove key (and its associated value) from the receiver. If key is not in
	the receiver, answer the result of evaluating aBlock. Otherwise, answer
	the value externally named by key."
	
	^ self subclassResponsibility! !

!AbstractEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/5/2020 14:03:19'!
classNamed: className 
	"className is either a class name or a class name followed by ' class'.
	Answer the class or metaclass it names.
	Answer nil if no class by that name.
	
	See SystemDictionary>>#classNamed:"

	^ self subclassResponsibility! !

!AbstractEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/5/2020 16:07:45'!
fillClassNameCache: aClassSet andNonClassNameCache: aNonClassSet
	"Ask the manager to fill the caches.
	
	See: SystemDictionary>>#fillCaches"

	^ self subclassResponsibility! !

!AbstractEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/5/2020 14:04:45'!
hasClassNamed: aString
	"Answer whether there is a class of the given name, but don't intern aString if it's not alrady interned.  4/29/96 sw
	
	See SystemDictionary>>#hasClassNamed:
	"

	^ self subclassResponsibility! !

!AbstractEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/5/2020 15:53:44'!
prepareToRenameClass: aClass as: newName 
	"Rename a class to a new name.

	See SystemDictionary>>#prepareToRenameClass:as: "

	^ self subclassResponsibility! !

!AbstractEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/5/2020 15:54:47'!
removeClassNamed: aName
	"Invoked from fileouts:  if there is currently a class in the system named aName, then remove it.  If anything untoward happens, report it in the Transcript.  
	
	Ask the manager to handle that case.
	
	See SystemDictionary>>#removeClassNamed:"
	
	self flag: #ProbablyOverkill.
	^ self subclassResponsibility

! !

!AbstractEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/5/2020 15:55:28'!
renameClassNamed: oldName as: newName
	"Invoked from fileouts:  if there is currently a class in the system named oldName, then rename it to newName.  If anything untoward happens, report it in the Transcript.  
	
	Ask the manager to handle that case.
	
	See SystemDictionary>>#renameClassNamed:as:"

	^ self subclassResponsibility! !

!AbstractEnvironmentManager methodsFor: 'housekeeping' stamp: 'KLG 10/5/2020 15:57:09'!
addObsoleteClassesTo: anOrderedCollection
	"Ask the manager to add their obsolete classes to anOrderedColletion.
	
	See SystemDictionary>>#obsoleteClasses"

	self subclassResponsibility! !

!AbstractEnvironmentManager methodsFor: 'housekeeping' stamp: 'KLG 10/5/2020 15:58:38'!
cleanOutUndeclared
	"Clean undeclared stuff in my environments.
	
	See See SystemDictionary>>#cleanOutUndeclared"
	
	self subclassResponsibility! !

!AbstractEnvironmentManager methodsFor: 'retrieving' stamp: 'KLG 10/5/2020 16:01:58'!
allBehaviorsDo: aBlock 
	"Evaluate the argument, aBlock, for each kind of Behavior in the system 
	(that is, Object and its subclasses).
	
	See SystemDictionary>>#allBehaviorsDo:"

	self subclassResponsibility! !

!AbstractEnvironmentManager methodsFor: 'scanning' stamp: 'KLG 10/17/2020 16:34:38'!
scanClassDenotation: aClassDenotation
	"Scann aClassDenotation, answer nil if the denotation is not handled by this environment implementation."
	

	^ self subclassResponsibility! !

!Object methodsFor: '*EnvironmentsBase' stamp: 'KLG 10/21/2020 16:41:00'!
environmentImplementationsIfPresentDo: aBlock
	"This method will execute aBlock with the single instance of the environment implementations directory.."
	
	^ aBlock value: EnvironmentImplementations ! !

!SystemDictionary methodsFor: '*EnvironmentsBase' stamp: 'KLG 11/4/2020 16:17:26'!
associationAt: key ifAbsent: aBlock 
	"Answer the association with the given key.
	If key is not found, return the result of evaluating aBlock."

	key forEnvironmentIfAware: [ :environment |
		^ environment associationAt: key ifAbsent: aBlock ].
	^ super associationAt: key ifAbsent: aBlock! !

!SystemDictionary methodsFor: '*EnvironmentsBase' stamp: 'KLG 10/12/2020 11:52:46'!
at: aKeySymbol
	"Answer the object at aKeySymbol"

	aKeySymbol doWithEnvironmentIfAware: [ :environment |
		^ environment at: aKeySymbol ].
	^ super at: aKeySymbol! !

!SystemDictionary methodsFor: '*EnvironmentsBase' stamp: 'KLG 10/12/2020 14:30:09'!
at: key ifAbsent: absentBlock
	"Answer the value associated with the key or, if key isn't found,
	answer the result of evaluating aBlock."

	key doWithEnvironmentIfAware: [ :environment |
		^ environment at: key ifAbsent: absentBlock ].
	^ super at: key ifAbsent: absentBlock! !

!SystemDictionary methodsFor: '*EnvironmentsBase' stamp: 'KLG 10/13/2020 11:55:34'!
at: key ifPresent: presentBlock ifAbsent: absentBlock
	"Answer the value associated with the key or, if key isn't found,
	answer the result of evaluating aBlock."
	"
		Smalltalk at: #zork ifPresent: [ :cls | (cls name, ' present') print ] ifAbsent: [ 'zork absent' print ]
		Smalltalk at: #Number ifPresent: [ :cls | (cls name, ' present') print ] ifAbsent: [ 'Number absent' print ]
	"

	key doWithEnvironmentIfAware: [ :environment |
		^ environment at: key  ifPresent: presentBlock ifAbsent: absentBlock ].
	^ super at: key ifPresent: presentBlock ifAbsent: absentBlock! !
EnvironmentAwareSystemOrganizer initialize!
EnvironmentImplementationsDictionary initialize!
