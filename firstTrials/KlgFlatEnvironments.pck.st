'From Cuis 5.0 [latest update: #4384] on 5 October 2020 at 6:47:02 pm'!
'Description I provide a very simple implementation of a flat, e.g. non nested environment.'!
!provides: 'KlgFlatEnvironments' 1 1!
!requires: 'EnvironmentsBase' 1 3 nil!
SystemOrganization addCategory: 'KlgFlatEnvironments'!


!classDefinition: #KlgFlatEnvMetaclass category: 'KlgFlatEnvironments'!
Metaclass subclass: #KlgFlatEnvMetaclass
	instanceVariableNames: 'environment'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgFlatEnvironments'!
!classDefinition: 'KlgFlatEnvMetaclass class' category: 'KlgFlatEnvironments'!
KlgFlatEnvMetaclass class
	instanceVariableNames: ''!

!classDefinition: #KlgFlatEnvironmentClassBuilder category: 'KlgFlatEnvironments'!
AbstractEnvironmentClassBuilder subclass: #KlgFlatEnvironmentClassBuilder
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgFlatEnvironments'!
!classDefinition: 'KlgFlatEnvironmentClassBuilder class' category: 'KlgFlatEnvironments'!
KlgFlatEnvironmentClassBuilder class
	instanceVariableNames: ''!

!classDefinition: #KlgFlatEnvEnvironmentManager category: 'KlgFlatEnvironments'!
AbstractEnvironmentManager subclass: #KlgFlatEnvEnvironmentManager
	instanceVariableNames: 'environments'
	classVariableNames: 'TheManager'
	poolDictionaries: ''
	category: 'KlgFlatEnvironments'!
!classDefinition: 'KlgFlatEnvEnvironmentManager class' category: 'KlgFlatEnvironments'!
KlgFlatEnvEnvironmentManager class
	instanceVariableNames: ''!

!classDefinition: #KlgFlatEnvEnvironment category: 'KlgFlatEnvironments'!
Object subclass: #KlgFlatEnvEnvironment
	instanceVariableNames: 'identifier bindings'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgFlatEnvironments'!
!classDefinition: 'KlgFlatEnvEnvironment class' category: 'KlgFlatEnvironments'!
KlgFlatEnvEnvironment class
	instanceVariableNames: ''!


!KlgFlatEnvMetaclass commentStamp: '<historical>' prior: 0!
I am a class in an environment.!

!KlgFlatEnvironmentClassBuilder commentStamp: '<historical>' prior: 0!
I am a class builder for classes in flat environments.!

!KlgFlatEnvEnvironmentManager commentStamp: '<historical>' prior: 0!
I am the manager for simple flat environments.!

!KlgFlatEnvEnvironment commentStamp: '<historical>' prior: 0!
I am a simple flat environment.!

!KlgFlatEnvMetaclass methodsFor: 'accessing' stamp: 'KLG 10/5/2020 16:33:20'!
environment
	"Answer the value of environment"

	^ environment! !

!KlgFlatEnvMetaclass methodsFor: 'class builder interface' stamp: 'KLG 10/5/2020 16:33:20'!
configureFromClassBuilder: aClassBuilder
	"Configure this environment from aClassBuilder"

	environment _ aClassBuilder environment! !

!KlgFlatEnvironmentClassBuilder methodsFor: 'instance creation' stamp: 'KLG 10/5/2020 18:23:41'!
metaclassClass
	"Answer the proper meta class for me."

	^ KlgFlatEnvMetaclass! !

!KlgFlatEnvironmentClassBuilder methodsFor: 'subclass hooks' stamp: 'KLG 10/5/2020 18:25:53'!
environmentFromSpecification: anEnvironmentSpecification
	"Answer the environment denoted by anEnvironmentSpecification.
	
	anEnvironmentSpecification is usallly a symbol or an array of symbols."

	^ self environmentManagerClass environment: anEnvironmentSpecification ! !

!KlgFlatEnvEnvironmentManager methodsFor: 'initialization' stamp: 'KLG 10/5/2020 16:39:54'!
initialize
	"Initialize the environment manager."

	super initialize.
	environments _ IdentityDictionary new	! !

!KlgFlatEnvEnvironmentManager methodsFor: 'instance creation' stamp: 'KLG 10/5/2020 18:34:15'!
environment: aNameSymbol
	"Answer the environment denoted by aNameSymbol.
	
	Create one if none exists."

	^ environments at: aNameSymbol ifAbsentPut: [
		self environmentClass named: aNameSymbol ]! !

!KlgFlatEnvEnvironmentManager methodsFor: 'instance creation' stamp: 'KLG 10/5/2020 16:38:22'!
environmentClass
	"Answer the class to be used for environments."

	^ self class environmentClass! !

!KlgFlatEnvEnvironmentManager class methodsFor: 'instance creation' stamp: 'KLG 10/5/2020 18:32:24'!
environment: aNameSymbol
	"Answer the environment denoted by aNameSymbol"

	^ TheManager environment: aNameSymbol! !

!KlgFlatEnvEnvironmentManager class methodsFor: 'instance creation' stamp: 'KLG 10/5/2020 16:38:22'!
environmentClass
	"Answer the class to be used for environments."

	^ KlgEnvEnvironment! !

!KlgFlatEnvEnvironmentManager class methodsFor: 'class initialization' stamp: 'KLG 10/5/2020 18:30:52'!
initialize
	"Create the single instance and install it in the environment implementations dictionary."

	super initialize.
	EnvironmentImplementations 
		at: #FlatEnvironments
		put: (TheManager _ self new)! !

!KlgFlatEnvEnvironment methodsFor: 'initialization' stamp: 'KLG 10/5/2020 16:43:56'!
initialize
	"Initialize the environment."

	super initialize.
	bindings _ IdentityDictionary new! !

!KlgFlatEnvEnvironment methodsFor: 'accessing' stamp: 'KLG 10/5/2020 16:45:41'!
identifier
	"Answer the environment's identifier"

	^ identifier! !

!KlgFlatEnvEnvironment methodsFor: 'accessing' stamp: 'KLG 10/5/2020 16:46:13'!
identifier: aNameSymbol
	"Set the environment's identifier"

	self assert: identifier isNil.
	identifier _ aNameSymbol! !

!KlgFlatEnvEnvironment class methodsFor: 'instance creation' stamp: 'KLG 10/5/2020 18:35:05'!
named: aNameSymbol
	"Answer a new environment name by aNameSymbol"

	^ self new identifier: aNameSymbol; yourself! !

!Class methodsFor: '*KlgFlatEnvironments' stamp: 'KLG 10/5/2020 18:43:03'!
subclass: t instanceVariableNames: f classVariableNames: d poolDictionaries: s category: cat inFlatEnvironment: e
	"This is the standard initialization message for creating a new class as a 
	subclass of an existing class (the receiver)."
	
	| answer |
	answer _ KlgFlatEnvironmentClassBuilder new
		superclass: self
		subclass: t
		instanceVariableNames: f
		classVariableNames: d
		poolDictionaries: s
		category: cat
		environment: e.
		
	Smalltalk
		logChange: answer definition 
		preamble: answer definitionPreamble.
	^answer
! !
KlgFlatEnvEnvironmentManager initialize!
