'From Cuis 5.0 [latest update: #4384] on 2 October 2020 at 1:00:19 pm'!
'Description Tests for KlgEnvironments.'!
!provides: 'KlgEnvironmentsTests' 1 13!
!requires: 'KlgEnvironments' 1 16 nil!
SystemOrganization addCategory: 'KlgEnvironmentsTests'!


!classDefinition: #KlgTEnvAbstractMetaclassTest category: 'KlgEnvironmentsTests'!
TestCase subclass: #KlgTEnvAbstractMetaclassTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgEnvironmentsTests'!
!classDefinition: 'KlgTEnvAbstractMetaclassTest class' category: 'KlgEnvironmentsTests'!
KlgTEnvAbstractMetaclassTest class
	instanceVariableNames: ''!

!classDefinition: #KlgTEnvTestOrdinaryClassCreation category: 'KlgEnvironmentsTests'!
KlgTEnvAbstractMetaclassTest subclass: #KlgTEnvTestOrdinaryClassCreation
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgEnvironmentsTests'!
!classDefinition: 'KlgTEnvTestOrdinaryClassCreation class' category: 'KlgEnvironmentsTests'!
KlgTEnvTestOrdinaryClassCreation class
	instanceVariableNames: ''!

!classDefinition: #KlgTEnvTestUseOfDerivedMetaclass category: 'KlgEnvironmentsTests'!
KlgTEnvAbstractMetaclassTest subclass: #KlgTEnvTestUseOfDerivedMetaclass
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgEnvironmentsTests'!
!classDefinition: 'KlgTEnvTestUseOfDerivedMetaclass class' category: 'KlgEnvironmentsTests'!
KlgTEnvTestUseOfDerivedMetaclass class
	instanceVariableNames: ''!

!classDefinition: #KlgTEnvTestEnvironmentCreation category: 'KlgEnvironmentsTests'!
TestCase subclass: #KlgTEnvTestEnvironmentCreation
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgEnvironmentsTests'!
!classDefinition: 'KlgTEnvTestEnvironmentCreation class' category: 'KlgEnvironmentsTests'!
KlgTEnvTestEnvironmentCreation class
	instanceVariableNames: ''!


!KlgTEnvTestOrdinaryClassCreation commentStamp: '<historical>' prior: 0!
Test Cuis core's ClassBuilder.

We mess(ed) arround with class builder, it need(s|ed) refactoring: So test it!!!

!KlgTEnvTestUseOfDerivedMetaclass commentStamp: '<historical>' prior: 0!
Check whether we can use instances of a subclass of Metaclass.!

!KlgTEnvTestEnvironmentCreation commentStamp: '<historical>' prior: 0!
Test various ways of creating environmentns.!

!KlgTEnvAbstractMetaclassTest methodsFor: 'setUp/tearDown' stamp: 'KLG 10/2/2020 12:04:22'!
setUp
	"Make sure we don't have our test class in the system."

	super setUp.
	Smalltalk at: #KlgEnvironmentTestClass ifPresent: [ :testClass |
		testClass removeFromSystem ]
		! !

!KlgTEnvAbstractMetaclassTest methodsFor: 'setUp/tearDown' stamp: 'KLG 10/2/2020 12:57:13'!
tearDown
	"Make sure we don't have our test class in the system."

	super tearDown.
	Smalltalk at: #KlgEnvironmentTestClass ifPresent: [ :testClass |
		testClass removeFromSystem ]
		! !

!KlgTEnvTestOrdinaryClassCreation methodsFor: 'testing' stamp: 'KLG 10/2/2020 12:07:33'!
testSimpleSubclassCreation
	"Just check that we can create a class with no environment."

	| newClass newMetaclass |
	newClass _ Object subclass: #KlgEnvironmentTestClass
		instanceVariableNames: ''
		classVariableNames: ''
		poolDictionaries: ''
		category: 'KlgEnvironmentTests'.
	self assert: newClass isBehavior.
	self assert: ((newMetaclass _ newClass class) isKindOf: Metaclass).
	"F: self assert: newMetaclass environment isNil"! !

!KlgTEnvTestUseOfDerivedMetaclass methodsFor: 'testing' stamp: 'KLG 9/30/2020 18:06:56'!
testSimpleSubclassCreation
	"Just check that we can create a class with no environment."

	| newClass newMetaclass |
	newClass _ Object subclass: #KlgEnvironmentTestClass
		instanceVariableNames: ''
		classVariableNames: ''
		poolDictionaries: ''
		category: 'KlgEnvironmentTests'
		environment: nil.
	self assert: newClass isBehavior.
	self assert: ((newMetaclass _ newClass class) isKindOf: KlgEnvMetaclass).
	self assert: newMetaclass environment isNil! !

!KlgTEnvTestUseOfDerivedMetaclass methodsFor: 'testing' stamp: 'KLG 9/30/2020 18:08:06'!
testSimpleSubclassCreationInEnvironment
	"Just check that we can create a class with no environment."

	| newClass newMetaclass |
	newClass _ Object subclass: #KlgEnvironmentTestClass
		instanceVariableNames: ''
		classVariableNames: ''
		poolDictionaries: ''
		category: 'KlgEnvironmentTests'
		environment: #TE1.
	self assert: newClass isBehavior.
	self assert: ((newMetaclass _ newClass class) isKindOf: KlgEnvMetaclass).
	self assert: newMetaclass environment == #TE1 asKlgEnvEnvironment! !

!KlgTEnvTestEnvironmentCreation methodsFor: 'testing' stamp: 'KLG 9/30/2020 17:59:38'!
testCreationFromArray
	"Create a sub environment form an array."

	| environment parentEnvironment |
	environment _ #(TE1 TE2) asKlgEnvEnvironment.
	self assert: (environment isKindOf: KlgEnvEnvironment).
	self assert: environment identifier equals: #TE2.
	self assert:  #(TE1 TE2) asKlgEnvEnvironment asKlgEnvEnvironment == environment.
	self assert: #TE1 equals: (parentEnvironment _ environment parent) identifier.
	self assert: parentEnvironment parent isNil.
	self assert: {#TE1. environment } asKlgEnvEnvironment == environment.
	self assert: {parentEnvironment. #TE2} asKlgEnvEnvironment == environment! !

!KlgTEnvTestEnvironmentCreation methodsFor: 'testing' stamp: 'KLG 9/30/2020 18:00:53'!
testCreationFromShit
	"Make sure environment creation from shit raises."

	self should: [ Object new asKlgEnvEnvironment ] raise: KlgEnvInvalidEnvironmentSpec.
	self should: [ #(1 2 3) asKlgEnvEnvironment ] raise:  KlgEnvInvalidEnvironmentSpec! !

!KlgTEnvTestEnvironmentCreation methodsFor: 'testing' stamp: 'KLG 9/30/2020 17:59:49'!
testCreationFromSymbol
	"Create a toplevel environment from a symbol."

	| environment |
	environment _ #TE1 asKlgEnvEnvironment.
	self assert: (environment isKindOf: KlgEnvEnvironment).
	self assert: environment identifier equals: #TE1.
	self assert: #TE1 asKlgEnvEnvironment == environment! !

!KlgTEnvTestEnvironmentCreation methodsFor: 'testing' stamp: 'KLG 9/30/2020 11:58:47'!
testNamedCreation
	"Created an environment with a name."

	| environment |
	self assert: ((environment _ KlgEnvEnvironment named: 'Sepp')
		isKindOf: KlgEnvEnvironment).
	self assert: #Sepp equals: environment identifier! !

!KlgTEnvTestEnvironmentCreation methodsFor: 'testing' stamp: 'KLG 9/30/2020 11:58:47'!
testSimpleCreation
	"Just make sure that we can create such a bugger."

	self assert: (KlgEnvEnvironment new isKindOf: KlgEnvEnvironment) ! !
