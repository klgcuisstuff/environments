'From Cuis 5.0 [latest update: #4384] on 5 October 2020 at 6:47:54 pm'!
'Description My take on environments aka. namespaces for Cuis.'!
!provides: 'KlgEnvironments' 1 18!
!requires: 'Cuis-Base' 50 4384 nil!
!requires: 'KLGCustomize' 1 3 nil!
SystemOrganization addCategory: 'KlgEnvironments'!


!classDefinition: #KlgEnvMetaclass category: 'KlgEnvironments'!
Metaclass subclass: #KlgEnvMetaclass
	instanceVariableNames: 'environment namespace'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgEnvironments'!
!classDefinition: 'KlgEnvMetaclass class' category: 'KlgEnvironments'!
KlgEnvMetaclass class
	instanceVariableNames: ''!

!classDefinition: #KlgEnvClassBuilder category: 'KlgEnvironments'!
ClassBuilder subclass: #KlgEnvClassBuilder
	instanceVariableNames: 'environment'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgEnvironments'!
!classDefinition: 'KlgEnvClassBuilder class' category: 'KlgEnvironments'!
KlgEnvClassBuilder class
	instanceVariableNames: ''!

!classDefinition: #KlgEnvInvalidEnvironmentSpec category: 'KlgEnvironments'!
Error subclass: #KlgEnvInvalidEnvironmentSpec
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgEnvironments'!
!classDefinition: 'KlgEnvInvalidEnvironmentSpec class' category: 'KlgEnvironments'!
KlgEnvInvalidEnvironmentSpec class
	instanceVariableNames: ''!

!classDefinition: #KlgEnvAbstractNamedEntity category: 'KlgEnvironments'!
Object subclass: #KlgEnvAbstractNamedEntity
	instanceVariableNames: 'identifier'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgEnvironments'!
!classDefinition: 'KlgEnvAbstractNamedEntity class' category: 'KlgEnvironments'!
KlgEnvAbstractNamedEntity class
	instanceVariableNames: ''!

!classDefinition: #KlgEnvEnvironment category: 'KlgEnvironments'!
KlgEnvAbstractNamedEntity subclass: #KlgEnvEnvironment
	instanceVariableNames: 'namespace interfaces imports exports parent children'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgEnvironments'!
!classDefinition: 'KlgEnvEnvironment class' category: 'KlgEnvironments'!
KlgEnvEnvironment class
	instanceVariableNames: 'manager'!

!classDefinition: #KlgEnvEnvironmentManager category: 'KlgEnvironments'!
Object subclass: #KlgEnvEnvironmentManager
	instanceVariableNames: 'toplevelEnvironments'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'KlgEnvironments'!
!classDefinition: 'KlgEnvEnvironmentManager class' category: 'KlgEnvironments'!
KlgEnvEnvironmentManager class
	instanceVariableNames: ''!


!KlgEnvMetaclass commentStamp: '<historical>' prior: 0!
I am a meta class in an environment.!

!KlgEnvInvalidEnvironmentSpec commentStamp: '<historical>' prior: 0!
I am the error signaled when we encounter an invalid environment specification.!

!KlgEnvAbstractNamedEntity commentStamp: '<historical>' prior: 0!
I am the abstract base class of all named entities.

I respond to #identifier and #identifier: to set the name.
!

!KlgEnvEnvironment commentStamp: '<historical>' prior: 0!
I am an environment that hides classes and global variables from the SystemDictionary singleton,
named Smalltalk.!

!KlgEnvMetaclass methodsFor: 'accessing' stamp: 'KLG 9/29/2020 20:08:09'!
environment
	"Answer the value of environment"

	^ environment! !

!KlgEnvMetaclass methodsFor: 'accessing' stamp: 'KLG 10/2/2020 12:28:29'!
environment: anObject
	"Set the value of environment"

	self deprecatedMethod. "Use configure from class builder instead!!"
	self assert: environment isNil.
	environment _ anObject! !

!KlgEnvMetaclass methodsFor: 'accessing' stamp: 'KLG 9/30/2020 11:49:18'!
namespace
	"Answer the classes namespace of imported classes (and such)."

	^ namespace ifNil: [ namespace _ IdentityDictionary new ]! !

!KlgEnvMetaclass methodsFor: 'class builder interface' stamp: 'KLG 10/2/2020 12:27:24'!
configureFromClassBuilder: aClassBuilder
	"Configure this environment from aClassBuilder"

	environment _ aClassBuilder environment! !

!KlgEnvClassBuilder methodsFor: 'public' stamp: 'KLG 9/30/2020 18:48:59'!
superclass: newSuper
	subclass: t instanceVariableNames: f 
	classVariableNames: d poolDictionaries: s category: cat 
	environment: e
	"This is the standard initialization message for creating a new class as a 
	subclass of an existing class in an environment."
	
	| answer |
	e ifNotNil: [ environment _ e asKlgEnvEnvironment ].
	answer _ 		super
		superclass: newSuper
		subclass: t instanceVariableNames: f 
		classVariableNames: d poolDictionaries: s category: cat .	
	^ answer! !

!KlgEnvClassBuilder methodsFor: 'subclass hooks' stamp: 'KLG 10/2/2020 12:53:03'!
configureNewMetaclass: aMetaclass
	"Configure a newly created  metaclass.
	
	The aMetaclass maybe the result of a shallow copy, so take care.
	
	Subclass me if you need special configuration."
	
	aMetaclass configureFromClassBuilder: self! !

!KlgEnvClassBuilder methodsFor: 'accessing' stamp: 'KLG 10/2/2020 12:55:16'!
environment
	"Answer the environment."

	^ environment ! !

!KlgEnvClassBuilder class methodsFor: 'instance creation' stamp: 'KLG 10/2/2020 12:11:03'!
metaclassClass
	"Answer the proper meta class for me."

	^ KlgEnvMetaclass! !

!KlgEnvAbstractNamedEntity methodsFor: 'accessing' stamp: 'KLG 9/30/2020 11:54:37'!
identifier
	"Answer the environments identifier."

	^identifier! !

!KlgEnvAbstractNamedEntity methodsFor: 'accessing' stamp: 'KLG 9/30/2020 11:54:50'!
identifier: mayBeSymbol 
	"Set the environments identifier."
	
	identifier _ mayBeSymbol asSymbol! !

!KlgEnvAbstractNamedEntity class methodsFor: 'instance creation' stamp: 'KLG 9/30/2020 11:55:05'!
named: aNameSymbol
	"Answer a new environment, identified by aNameSymbol."

	^ self new identifier: aNameSymbol 
	! !

!KlgEnvEnvironment methodsFor: 'initialization' stamp: 'KLG 9/29/2020 16:20:31'!
initialize
	"Initialize an environment."

	self flagKLG: 'XXX: interfaces need defaults'.
	super initialize.
	namespace _ IdentityDictionary new.
	interfaces _ IdentityDictionary new.
	exports _ IdentityDictionary new.! !

!KlgEnvEnvironment methodsFor: 'accessing' stamp: 'KLG 10/3/2020 06:04:22'!
at: aNameSymbol put: anObject
	"Put an object into my namespace."

	^ namespace at: aNameSymbol put: anObject! !

!KlgEnvEnvironment methodsFor: 'accessing' stamp: 'KLG 10/3/2020 07:24:39'!
at: aNameSymbol putClass: aClass
	"Put a class into my namespace.
	
	Just an alias for #at:put:, but enables my subclasses to treat classes differently."

	^ self at: aNameSymbol put: aClass! !

!KlgEnvEnvironment methodsFor: 'accessing' stamp: 'KLG 9/30/2020 15:41:18'!
children
	"Answer the dictionary with teh children."
	
	^ children ifNil: [ children _ IdentityDictionary new ]! !

!KlgEnvEnvironment methodsFor: 'accessing' stamp: 'KLG 9/30/2020 12:51:30'!
parent
	"Answer the parent."

	^ parent! !

!KlgEnvEnvironment methodsFor: 'accessing' stamp: 'KLG 9/30/2020 12:51:10'!
parent: anEnvironment
	"Set the parent."

	self assert: parent isNil.
	parent _ anEnvironment! !

!KlgEnvEnvironment methodsFor: 'accessing' stamp: 'KLG 9/30/2020 12:47:30'!
subEnvironment: aNameSymbol
	"Answer the sub-environment denoted by aNameSymbol.
	
	Create it if not available."

	| cs |
	^ (cs _ self children) at: aNameSymbol ifAbsent: [
		cs 
			at: aNameSymbol 
			put: (self subEnvironmentClass
					withParent: self 
					andIdentifier: aNameSymbol) ]! !

!KlgEnvEnvironment methodsFor: 'instance creation' stamp: 'KLG 9/30/2020 12:37:01'!
subEnvironmentClass
	"Answer the class for sub environments."

	^ self class subEnvironmentClass! !

!KlgEnvEnvironment methodsFor: 'conversion' stamp: 'KLG 9/30/2020 13:43:53'!
asKlgEnvEnvironment
	"I am already an environment."

	^ self! !

!KlgEnvEnvironment methodsFor: 'conversion' stamp: 'KLG 9/30/2020 17:23:38'!
asKlgEnvPathComponent
	"Answer my identifier as a path component."

	identifier ifNil: [
		KlgEnvInvalidEnvironmentSpec
			signal: 'Can''t use anonymous environments as path components.' ].
	^ identifier ! !

!KlgEnvEnvironment class methodsFor: 'instance creation' stamp: 'KLG 9/30/2020 11:57:11'!
environmentManagerClass
	"Answer the class of the environment manager."

	^ KlgEnvEnvironmentManager! !

!KlgEnvEnvironment class methodsFor: 'instance creation' stamp: 'KLG 9/30/2020 15:31:16'!
fromPath: aPathArray
	"Answer a new environment, identified by aPathArray."

	^ self manager environment: aPathArray! !

!KlgEnvEnvironment class methodsFor: 'instance creation' stamp: 'KLG 9/30/2020 12:24:20'!
named: aNameSymbol
	"Answer a new environment, identified by aNameSymbol."

	^ self manager toplevelEnvironment: aNameSymbol ! !

!KlgEnvEnvironment class methodsFor: 'instance creation' stamp: 'KLG 9/30/2020 12:37:12'!
subEnvironmentClass
	"Answer the class for sub environments."

	^ self! !

!KlgEnvEnvironment class methodsFor: 'instance creation' stamp: 'KLG 9/30/2020 15:34:38'!
withParent: aParent andIdentifier: mayBeNameSymbol
	"Answer a sub environment of aParent identifed by aNameSymbol."

	^ self new parent: aParent; identifier: mayBeNameSymbol asKlgEnvPathComponent; yourself! !

!KlgEnvEnvironment class methodsFor: 'accessing' stamp: 'KLG 9/30/2020 10:51:10'!
manager
	"Answer the environment manager."

	^ manager ifNil: [ manager _ self environmentManagerClass new ]! !

!KlgEnvEnvironmentManager methodsFor: 'initialization' stamp: 'KLG 9/30/2020 10:48:10'!
initialize
	"Initialize the environment manager."

	super initialize.
	toplevelEnvironments _ IdentityDictionary new	! !

!KlgEnvEnvironmentManager methodsFor: 'access' stamp: 'KLG 9/30/2020 17:18:42'!
environment: aPathArray
	"Answer the environment denoted by aPathArray"

	| answer |
	answer _ self toplevelEnvironment: aPathArray first.
	aPathArray allButFirstDo: [ :mayBeNameSymbol |
		answer _ answer subEnvironment: mayBeNameSymbol asKlgEnvPathComponent ].
	^ answer! !

!KlgEnvEnvironmentManager methodsFor: 'access' stamp: 'KLG 9/30/2020 17:27:24'!
toplevelEnvironment: mayBeNameSymbol
	"Answer the toplevel environment denoted by a name symbol.
	Create it if not present."

	| nameSymbol |
	^ toplevelEnvironments at: (nameSymbol _ mayBeNameSymbol asKlgEnvPathComponent) ifAbsent: [
		toplevelEnvironments 
			at: nameSymbol
			put: (self environmentClass new identifier: nameSymbol) ]! !

!KlgEnvEnvironmentManager methodsFor: 'instance creation' stamp: 'KLG 9/30/2020 12:14:18'!
environmentClass
	"Answer the class to be used for environments."

	^ self class environmentClass! !

!KlgEnvEnvironmentManager class methodsFor: 'instance creation' stamp: 'KLG 9/30/2020 12:14:30'!
environmentClass
	"Answer the class to be used for environments."

	^ KlgEnvEnvironment! !

!Object methodsFor: '*KlgEnvironments' stamp: 'KLG 9/30/2020 15:36:02'!
asKlgEnvEnvironment
	"Raise an appropriate error."

	^ KlgEnvInvalidEnvironmentSpec signal: 'I must not occur in an environment specification'! !

!Object methodsFor: '*KlgEnvironments' stamp: 'KLG 9/30/2020 15:36:09'!
asKlgEnvPathComponent
	"Raise an appropriate error."

	^ KlgEnvInvalidEnvironmentSpec signal: 'I must not occur in an environment path specification'! !

!Class methodsFor: '*KlgEnvironments' stamp: 'KLG 9/30/2020 11:58:00'!
subclass: t instanceVariableNames: f classVariableNames: d poolDictionaries: s category: cat environment: e
	"This is the standard initialization message for creating a new class as a 
	subclass of an existing class (the receiver)."
	
	| answer |
	answer _ KlgEnvClassBuilder new
		superclass: self
		subclass: t
		instanceVariableNames: f
		classVariableNames: d
		poolDictionaries: s
		category: cat
		environment: e.
		
	Smalltalk
		logChange: answer definition 
		preamble: answer definitionPreamble.
	^answer
! !

!SequenceableCollection methodsFor: '*KlgEnvironments' stamp: 'KLG 9/30/2020 15:40:01'!
asKlgEnvEnvironment
	"Answer the environment speci"

	^ KlgEnvEnvironment fromPath: self ! !

!String methodsFor: '*KlgEnvironments' stamp: 'KLG 9/30/2020 17:31:15'!
asKlgEnvEnvironment
	"Answer a toplevel environment identified by me."
	
	^ self asSymbol ! !

!String methodsFor: '*KlgEnvironments' stamp: 'KLG 9/30/2020 17:32:45'!
asKlgEnvPathComponent
	"Answer me as a path component."

	^ self asSymbol! !

!Symbol methodsFor: '*KlgEnvironments' stamp: 'KLG 9/30/2020 12:21:21'!
asKlgEnvEnvironment
	"Answer a toplevel environment identified by me."
	
	^ KlgEnvEnvironment named: self! !

!Symbol methodsFor: '*KlgEnvironments' stamp: 'KLG 9/30/2020 17:20:33'!
asKlgEnvPathComponent
	"Answer me as a path component."

	^ self! !
