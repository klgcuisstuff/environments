'From Cuis 5.0 [latest update: #4438] on 6 November 2020 at 9:45:41 pm'!
'Description A simple environments implementation, meant as an example and a base to inherit from for your own implementations.'!
!provides: 'SimpleEnvironments' 1 27!
!requires: 'EnvironmentsBase' 1 21 nil!
SystemOrganization addCategory: 'SimpleEnvironments'!


!classDefinition: #SimpleEnvironmentMetaclass category: 'SimpleEnvironments'!
Metaclass subclass: #SimpleEnvironmentMetaclass
	instanceVariableNames: 'environment'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SimpleEnvironments'!
!classDefinition: 'SimpleEnvironmentMetaclass class' category: 'SimpleEnvironments'!
SimpleEnvironmentMetaclass class
	instanceVariableNames: ''!

!classDefinition: #SimpleEnvironmentClassBuilder category: 'SimpleEnvironments'!
AbstractEnvironmentClassBuilder subclass: #SimpleEnvironmentClassBuilder
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SimpleEnvironments'!
!classDefinition: 'SimpleEnvironmentClassBuilder class' category: 'SimpleEnvironments'!
SimpleEnvironmentClassBuilder class
	instanceVariableNames: ''!

!classDefinition: #SimpleEnvironment category: 'SimpleEnvironments'!
AbstractEnvironment subclass: #SimpleEnvironment
	instanceVariableNames: 'name bindings'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SimpleEnvironments'!
!classDefinition: 'SimpleEnvironment class' category: 'SimpleEnvironments'!
SimpleEnvironment class
	instanceVariableNames: ''!

!classDefinition: #SimpleEnvironmentManager category: 'SimpleEnvironments'!
AbstractEnvironmentManager subclass: #SimpleEnvironmentManager
	instanceVariableNames: 'environments'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SimpleEnvironments'!
!classDefinition: 'SimpleEnvironmentManager class' category: 'SimpleEnvironments'!
SimpleEnvironmentManager class
	instanceVariableNames: 'singleInstance'!


!SimpleEnvironmentMetaclass commentStamp: '<historical>' prior: 0!
I am a meta class in an environment.!

!SimpleEnvironmentClassBuilder commentStamp: '<historical>' prior: 0!
I am a class builder for classes in simple environments.!

!SimpleEnvironment commentStamp: '<historical>' prior: 0!
I am a simple environment.!

!SimpleEnvironmentManager commentStamp: '<historical>' prior: 0!
I am a simple environment manger that hold all its environments in
an identity dictionary.

I can be used as superclass for more sophisticated implementations.!

!SimpleEnvironmentMetaclass methodsFor: 'fileIn/Out' stamp: 'KLG 10/12/2020 18:05:23'!
printAdditionalDefinitionOn: aStream
	"Print additional defintion information on aStream.

	Print the environment to use."

	aStream
		newLine;
		tab;
		nextPutAll: 'inSimpleEnvironment: ';
		store: environment name.! !

!SimpleEnvironmentMetaclass methodsFor: 'compiling' stamp: 'KLG 10/6/2020 20:06:52'!
forNonLocalBindingEnvironmentsDo: aBlock
	"Evaluate aBlock for all non local environments."

	aBlock value: environment.
	super forNonLocalBindingEnvironmentsDo: aBlock! !

!SimpleEnvironmentMetaclass methodsFor: 'printing' stamp: 'KLG 10/8/2020 16:43:47'!
denotingExpression
	"Answer a String that is the name of the receiver, either 'Metaclass' or 
	the name of the receiver's class followed by ' class'.
	
	Keep in mind that 'thisClass denotingExpression' might need
	parenthesis in subclasses."

	^ thisClass
		ifNil: [ super denotingExpression ]
		ifNotNil: [ '(', thisClass denotingExpression, ') class' ]! !

!SimpleEnvironmentMetaclass methodsFor: 'printing' stamp: 'KLG 10/12/2020 11:58:36'!
denotingExpressionForClass
	"Answer a string that evaluates to this class.
	
	Can be redefined in subclasses to prorvide support for
	navigating to a class in an environment."

	^ environment printString, '>>#', super denotingExpressionForClass ! !

!SimpleEnvironmentMetaclass methodsFor: 'printing' stamp: 'KLG 10/21/2020 17:16:48'!
denotingLiteral
	"Answer a literal that denotes this class
	
	We DO NOT answer a literal, the scanning code for simple environments can deal with this."

	^ self denotingExpression! !

!SimpleEnvironmentMetaclass methodsFor: 'printing' stamp: 'KLG 10/21/2020 17:19:27'!
denotingLiteralForClass
	"Answer denoting literal.
	
	The scanning code can deal with expressions
	
	Can be redefined in subclasses to prorvide support for
	navigating to a class in an environment."

	^ self denotingExpressionForClass! !

!SimpleEnvironmentMetaclass methodsFor: 'printing' stamp: 'KLG 10/12/2020 18:18:59'!
safeDenotingExpressionForClass
	"Answer a string that evaluates to this class.
	
	Add parentheses, because it is a complex string."

	^ '(', self denotingExpressionForClass, ')'! !

!SimpleEnvironmentMetaclass methodsFor: 'printing' stamp: 'KLG 10/21/2020 17:17:01'!
safeDenotingLiteral
	"Answer a literal that denotes this class
	
	We DO NOT answer a literal, the scanning code for simple environments can deal with this."

	^ self safeDenotingExpression! !

!SimpleEnvironmentMetaclass methodsFor: 'printing' stamp: 'KLG 10/21/2020 17:20:33'!
safeDenotingLiteralForClass
	"Answer a string that denotes this class.
	
	The scanning code can deal with expression.
	
	Add parentheses, because it is a complex string."

	^ self safeDenotingExpressionForClass! !

!SimpleEnvironment methodsFor: 'accessing' stamp: 'KLG 10/6/2020 15:29:27'!
name
	"Answer the environment's identifier"

	^ name! !

!SimpleEnvironment methodsFor: 'compiling' stamp: 'KLG 10/6/2020 15:32:44'!
bindingOf: aVarName
	"Answer the binding of a variable name.
	
	Answer nil if the variable is not bound in this environment."

	^ bindings bindingOf: aVarName! !

!SimpleEnvironment methodsFor: 'shrinking' stamp: 'KLG 10/6/2020 18:42:40'!
removeSelector: descriptor
	"Safely remove a selector from a class (or metaclass). If the class
	or the method doesn't exist anymore, never mind and answer nil.
	This method should be used instead of 'Class removeSelector: #method'
	to omit global class references."

	| class sel |
	class _ bindings at: descriptor first ifAbsent: [ ^ nil ].
	descriptor size > 2 and: [ descriptor second == #class ] ::
		ifTrue: [
			class _ class class.
			sel _ descriptor third ]
		ifFalse: [
			sel _ descriptor second ].
	^ class removeSelector: sel! !

!SimpleEnvironment methodsFor: 'denoting objects' stamp: 'KLG 10/8/2020 17:39:14'!
>> aKey
	"Answer the object at aKey."

	^ self at: aKey! !

!SimpleEnvironment methodsFor: 'printing' stamp: 'KLG 10/8/2020 16:40:45'!
printOn: aStream
	"Print an expression that evaluates to me on aStream."

	aStream 
		nextPutAll: self managerClass singleInstanceGlobalName;
		nextPutAll: '>>#';
		nextPutAll: self name! !

!SimpleEnvironmentManager methodsFor: 'shrinking' stamp: 'KLG 10/6/2020 14:47:08'!
removeSelector: descriptor
	"Safely remove a selector from a class (or metaclass). If the class
	or the method doesn't exist anymore, never mind and answer nil.
	This method should be used instead of 'Class removeSelector: #method'
	to omit global class references.
	
	Answer the behaviour handling this remove request,
	otherwise answer nil.
	
	See: See SystemDictionary>>#removeSelector:."
	
	environments do: [ :environment |
		environment removeSelector: descriptor :: ifNotNil: [  :affectedBehaviour |
			^ affectedBehaviour ] ].
	^ nil
! !

!SimpleEnvironmentManager methodsFor: 'denoting objects' stamp: 'KLG 10/12/2020 18:23:25'!
>> anEnvironmentName
	"Answer the environment denoted by anEnvironmentName."

	
	^ environments		at: (self convertToEnvironmentName: anEnvironmentName)! !

!SimpleEnvironmentMetaclass methodsFor: 'accessing' stamp: 'KLG 10/6/2020 19:18:36'!
environment
	"Answer the value of environment"

	^ environment! !

!SimpleEnvironmentMetaclass methodsFor: 'class builder interface' stamp: 'KLG 10/6/2020 19:18:36'!
configureFromClassBuilder: aClassBuilder
	"Configure this environment from aClassBuilder"

	environment _ aClassBuilder environment! !

!SimpleEnvironmentClassBuilder methodsFor: 'instance creation' stamp: 'KLG 10/7/2020 15:44:07'!
environmentManagerClass
	"Answer the environment manager class."

	^ self class environmentManagerClass! !

!SimpleEnvironmentClassBuilder methodsFor: 'subclass hooks' stamp: 'KLG 10/7/2020 15:39:39'!
environmentFromSpecification: anEnvironmentSpecification
	"Answer the environment denoted by anEnvironmentSpecification.
	
	anEnvironmentSpecification is usallly a symbol or an array of symbols."

	^ self environmentManagerClass environment: anEnvironmentSpecification ! !

!SimpleEnvironmentClassBuilder class methodsFor: 'instance creation' stamp: 'KLG 10/8/2020 11:49:04'!
environmentManagerClass
	"Answer the environment manager class."

	^ SimpleEnvironmentManager ! !

!SimpleEnvironmentClassBuilder class methodsFor: 'instance creation' stamp: 'KLG 10/29/2020 18:06:25'!
metaclassClass
	"Answer the proper meta class for me."

	^ SimpleEnvironmentMetaclass! !

!SimpleEnvironment methodsFor: 'accessing' stamp: 'KLG 11/4/2020 16:18:47'!
associationAt: key ifAbsent: aBlock 
	"Answer the association with the given key.
	If key is not found, return the result of evaluating aBlock."

	^ bindings associationAt: key ifAbsent: aBlock! !

!SimpleEnvironment methodsFor: 'accessing' stamp: 'KLG 11/6/2020 21:29:14'!
associations
	"Answer the associations"

	^ bindings associations! !

!SimpleEnvironment methodsFor: 'accessing' stamp: 'KLG 10/7/2020 16:22:59'!
at: aKeySymbol
	"Answer the object at aKeySymbol"

	^ bindings at: aKeySymbol! !

!SimpleEnvironment methodsFor: 'accessing' stamp: 'KLG 10/7/2020 16:23:27'!
at: aKeySymbol ifAbsent: aBlock
	"Answer the object at aKeySymbol"

	^ bindings at: aKeySymbol ifAbsent: aBlock! !

!SimpleEnvironment methodsFor: 'accessing' stamp: 'KLG 10/7/2020 16:23:38'!
at: aKeySymbol ifAbsentPut: aBlock
	"Answer the object at aKeySymbol"

	^ bindings at: aKeySymbol ifAbsentPut: aBlock! !

!SimpleEnvironment methodsFor: 'accessing' stamp: 'KLG 10/8/2020 17:54:29'!
at: aKey ifPresent: aBlock
	"Execute aBlock if aKey is found."

	^ bindings at: aKey ifPresent: aBlock ifAbsent: [ nil ]! !

!SimpleEnvironment methodsFor: 'accessing' stamp: 'KLG 10/13/2020 11:57:28'!
at: key ifPresent: presentBlock ifAbsent: absentBlock
	"Answer the value associated with the key or, if key isn't found,
	answer the result of evaluating aBlock."

	^ bindings at: key ifPresent: presentBlock ifAbsent: absentBlock! !

!SimpleEnvironment methodsFor: 'accessing' stamp: 'KLG 10/7/2020 16:22:23'!
at: aKeySymbol put: anObject
	"Put anObject into the environment."

	^ bindings at: aKeySymbol put: anObject! !

!SimpleEnvironment methodsFor: 'accessing' stamp: 'KLG 10/6/2020 15:29:51'!
name: aNameSymbol
	"Set the environment's identifier"

	self assert: name isNil.
	name _ aNameSymbol! !

!SimpleEnvironment methodsFor: 'manager' stamp: 'KLG 10/6/2020 17:29:29'!
managerClass
	"Answer the environment manager class."

	^ self class managerClass	! !

!SimpleEnvironment methodsFor: 'initialization' stamp: 'KLG 10/12/2020 10:16:20'!
initialize
	"Initialize the environment."

	super initialize.
	bindings _ EnvironmentBindingIdentityDictionary forEnvironment: self! !

!SimpleEnvironment methodsFor: 'class names' stamp: 'KLG 10/25/2020 21:46:59'!
classNamed: className 
	"className is either a class name or a class name followed by ' class'.
	Answer the class or metaclass it names.
	Answer nil if no class by that name."
	"
	Smalltalk classNamed: #Point
	Smalltalk classNamed: 'Point'
	Smalltalk classNamed: 'Point class'
	Smalltalk classNamed: 'BogusClassName'
	Smalltalk classNamed: 'BogusClassName class'

	Smalltalk classNamed: #Display
	Smalltalk classNamed: 'Display'
	Smalltalk classNamed: 'Display class'
	"
	
	"self flagKLG: 'XXX: Revisit if we have some syntax to denote classes in this environment implemenations'.
	self flagKLG: 'XXX: Used from CodeFileBrowser.'."
	self flag: #flagKLG:.
	bindings at: (self convertToEnvironmentName: className) ifPresent: [ :found |
		^ found isBehavior ifTrue: [ found ] ].
	(className withoutSuffix: ' class') ifNotNil: [ :baseName |
		bindings at: (self convertToEnvironmentName: baseName) ifPresent: [ :found |
			^ found isBehavior ifTrue: [ found class ]]].
	^ nil! !

!SimpleEnvironment methodsFor: 'class names' stamp: 'KLG 10/8/2020 17:30:29'!
fillClassNameCache: aClassSet andNonClassNameCache: aNonClassSet
	"Fill the caches.	

	See: SystemDictionary>>#fillCaches"

	bindings do: [ :value | 
		value isBehavior			ifTrue: [ aClassSet ] ifFalse: [ aNonClassSet ] :: add: value name ]
	
! !

!SimpleEnvironment methodsFor: 'class names' stamp: 'KLG 10/6/2020 17:43:54'!
hasClassNamed: aString
	"Answer whether there is a class of the given name, but don't intern aString if it's not alrady interned.  4/29/96 sw
	Smalltalk hasClassNamed: 'Morph'
	Smalltalk hasClassNamed: 'Display'
	Smalltalk hasClassNamed: 'xMorph'
	"

	Symbol hasInterned: aString ifTrue: [ :aSymbol |
		bindings at: aSymbol ifPresent: [ :object | ^ object class isMeta ]].
	^ false! !

!SimpleEnvironment methodsFor: 'class names' stamp: 'KLG 10/7/2020 12:59:49'!
prepareToRenameClass: aClass as: newName 
	"Rename a class to a new name.

	See SystemDictionary>>#prepareToRenameClass:as: "

	| oldref category oldName |
	oldName _ aClass name.
	bindings at: oldName ifAbsent: [ ^ false ].
	category _ SystemOrganization categoryOfElement: oldName.
	SystemOrganization classify: newName under: category.
	SystemOrganization removeElement: oldName.
	oldref _ bindings associationAt: oldName.
	bindings removeKey: oldName.
	oldref key: newName.
	bindings add: oldref.  "Old association preserves old refs"
	self flag: #StartUpList.
	self flag: #ShutdownList.
	Smalltalk flushClassNameCache.
	SystemChangeNotifier uniqueInstance 
		aboutToRenameClass: aClass from: oldName to: newName inCategory: category.
	^ true! !

!SimpleEnvironment methodsFor: 'class names' stamp: 'KLG 10/6/2020 18:10:48'!
removeClassNamed: aName
	"Invoked from fileouts:  if there is currently a class in the system named aName, then remove it.  If anything untoward happens, report it in the Transcript.  "

	| oldClass |
	oldClass _ bindings at: aName asSymbol ifAbsent: [
		Transcript 
			newLine; 
			nextPutAll: 'Removal of class named ';
			nextPutAll: aName;
			nextPutAll: ' from environment ';
			nextPutAll: self name;
			nextPutAll: 		' ignored because ';
			nextPutAll: aName;
			nextPutAll: ' does not exist.'.
		^ false].

	oldClass removeFromSystem.
	^ true! !

!SimpleEnvironment methodsFor: 'class names' stamp: 'KLG 10/6/2020 18:24:12'!
renameClassNamed: oldName as: newName
	"Invoked from fileouts:  if there is currently a class in the system named oldName, then rename it to newName.  If anything untoward happens, report it in the Transcript.  
	
	Ask the manager to handle that case.
	
	See SystemDictionary>>#renameClassNamed:as:"

	| oldClass |
	oldClass _ self at: oldName asSymbol ifAbsent: [
		Transcript 
			newLine; 
			nextPutAll: 'Class-rename for ';
			nextPutAll: oldName;
			nextPutAll: ' ignored because ';
			nextPutAll: oldName;
			nextPutAll: ' does not exist in environment ';
			nextPutAll: self name;
			nextPut: $. .
		^ false ].
	oldClass rename: newName.
	^ true! !

!SimpleEnvironment methodsFor: 'housekeeping' stamp: 'KLG 10/25/2020 21:45:01'!
addObsoleteClassesTo: anOrderedCollection
	"Ask the manager to add their obsolete classes to anOrderedColletion.
	
	See SystemDictionary>>#obsoleteClasses"

	bindings do: [ :value |
		value class isMeta and: [ 'AnOb*' match: value name asString  ] :: ifTrue: [
			anOrderedCollection add: value ] ]! !

!SimpleEnvironment methodsFor: 'housekeeping' stamp: 'KLG 10/6/2020 18:30:39'!
cleanOutUndeclared
	"Remove all unreferences classes.
	
	We don't manage undeclared objects."
	! !

!SimpleEnvironment methodsFor: 'retrieving' stamp: 'KLG 10/6/2020 18:33:01'!
addToPoolUsers: aDictionary
	"Add references to shared pools to aDictionary.
	
	See SystemDictionary>>#poolUsers
	
	Probably unneeded."

	! !

!SimpleEnvironment methodsFor: 'retrieving' stamp: 'KLG 10/6/2020 18:39:04'!
allBehaviorsDo: aBlock 
	"Evaluate the argument, aBlock, for each kind of Behavior in the system 
	(that is, Object and its subclasses).
	ar 7/15/1999: The code below will not enumerate any obsolete or anonymous
	behaviors for which the following should be executed:

		Smalltalk allObjectsDo:[:obj| obj isBehavior ifTrue:[aBlock value: obj]].

	but what follows is way faster than enumerating all objects."

	 "This will only do any thing if some one places a class with superclass nil in me:"
	self flag: #ProbablyOverkill.
	bindings do: [ :root |
		(root isBehavior and: [root superclass isNil]) ifTrue: [	"Grab ProtoObject and any other alike"
			root withAllSubclassesDo: [ :class |
				class isMeta ifFalse: [ "The metaclasses are rooted at Class; don't include them twice."
					aBlock
						value: class;
						value: class class ]]]]! !

!SimpleEnvironment methodsFor: 'conversion' stamp: 'KLG 10/6/2020 18:04:01'!
convertToEnvironmentName: anObject
	"Convert an object to a suitable envernment specification.
	
	May be redefined in subclasses."
	
	^ self managerClass convertToEnvironmentName: anObject! !

!SimpleEnvironment methodsFor: 'removing' stamp: 'KLG 10/14/2020 09:47:43'!
removeKey: key ifAbsent: aBlock
	"Remove key (and its associated value) from the receiver. If key is not in
	the receiver, answer the result of evaluating aBlock. Otherwise, answer
	the value externally named by key."

	^ bindings removeKey: key ifAbsent: aBlock! !

!SimpleEnvironment class methodsFor: 'instance creation' stamp: 'KLG 10/6/2020 15:29:51'!
named: aNameSymbol
	"Answer a new environment name by aNameSymbol"

	^ self new name: aNameSymbol; yourself! !

!SimpleEnvironment class methodsFor: 'manager' stamp: 'KLG 10/8/2020 11:49:04'!
managerClass
	"Answer the environment manager class."

	^ SimpleEnvironmentManager! !

!SimpleEnvironmentManager methodsFor: 'initialization' stamp: 'KLG 10/6/2020 14:47:07'!
initialize
	"Initialize the environment manager."

	super initialize.
	environments _ IdentityDictionary new	! !

!SimpleEnvironmentManager methodsFor: 'instance creation' stamp: 'KLG 10/6/2020 14:47:07'!
environmentClass
	"Answer the class to be used for environments."

	^ self class environmentClass! !

!SimpleEnvironmentManager methodsFor: 'conversion' stamp: 'KLG 11/3/2020 16:26:00'!
convertToEnvironmentName: anObject
	"Convert an object to a suitable environment specification.
	
	May be redefined in subclasses."
	
	^ self class convertToEnvironmentName: anObject! !

!SimpleEnvironmentManager methodsFor: 'accessing' stamp: 'KLG 10/8/2020 18:10:50'!
atAllEnvironments: aKey ifPresent: aBlock
	"Find a key in all implementations, if present execute aBock with its value."

	environments do: [ :environment | 		environment at: aKey ifPresent: aBlock ]! !

!SimpleEnvironmentManager methodsFor: 'accessing' stamp: 'KLG 11/4/2020 15:11:37'!
environment: aName
	"Answer the environment denoted by aName.
	
	Create one if none exists."

	| nameSymbol |
	^ environments
		at: (nameSymbol _ self convertToEnvironmentName: aName)
		ifAbsentPut: [ self environmentClass named: nameSymbol ].! !

!SimpleEnvironmentManager methodsFor: 'accessing' stamp: 'KLG 11/6/2020 10:43:44'!
environments
	"Answer a collection of known environments."

	^ environments values! !

!SimpleEnvironmentManager methodsFor: 'accessing' stamp: 'KLG 11/4/2020 15:11:25'!
removeEnvironment: aName
	"Answer the environment denoted by aName."
	
	^ environments removeKey:  (self convertToEnvironmentName: aName)! !

!SimpleEnvironmentManager methodsFor: 'testing' stamp: 'KLG 10/8/2020 11:30:19'!
isMyClass: aClass
	"Answer true if aClass belongs into my environments."

	^ aClass isKindOf: SimpleEnvironmentMetaclass! !

!SimpleEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/6/2020 14:47:08'!
classNamed: className 
	"className is either a class name or a class name followed by ' class'.
	Answer the class or metaclass it names.
	Answer nil if no class by that name.
	
	See SystemDictionary>>#classNamed:"

	environments do: [ :environment |
		environment classNamed: className :: ifNotNil: [ :class |
			^ class ] ].
	^ nil! !

!SimpleEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/6/2020 14:47:08'!
fillClassNameCache: aClassSet andNonClassNameCache: aNonClassSet
	"Ask the manager to fill the caches.
	
	See: SystemDictionary>>#fillCaches"

	^ environments do: [ :environment |
		environment fillClassNameCache: aClassSet andNonClassNameCache: aNonClassSet ]! !

!SimpleEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/6/2020 14:47:08'!
hasClassNamed: aString
	"Answer whether there is a class of the given name, but don't intern aString if it's not alrady interned.  4/29/96 sw
	
	See SystemDictionary>>#hasClassNamed:
	"

	environments do: [ :environment |
		environment hasClassNamed: aString :: ifTrue: [ ^ true ] ].
	^ false! !

!SimpleEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/7/2020 16:48:57'!
prepareToRenameClass: aClass as: newName 
	"Rename a class to a new name.
	
	Ask all environments to handle that request.
	See SystemDictionary>>#prepareToRenameClass:as: "

	self isMyClass: aClass :: ifTrue: [
		^ environments
			at: aClass class environment name 
			ifPresent: [ :environment |
				environment prepareToRenameClass: aClass as: newName ]
			ifAbsent: [ false ] ].
	^ false! !

!SimpleEnvironmentManager methodsFor: 'class names' stamp: 'KLG 10/6/2020 14:47:08'!
renameClassNamed: oldName as: newName
	"Invoked from fileouts:  if there is currently a class in the system named oldName, then rename it to newName.  If anything untoward happens, report it in the Transcript.  
	
	Ask the environments to handle that case.
	
	See SystemDictionary>>#renameClassNamed:as:"

	environments do: [ :environment |
		environment renameClassNamed: oldName as: newName :: ifTrue: [ ^ true ] ].
	^ false! !

!SimpleEnvironmentManager methodsFor: 'housekeeping' stamp: 'KLG 10/6/2020 14:47:08'!
addObsoleteClassesTo: anOrderedCollection
	"Ask all environments to add their obsolete classes to anOrderedColletion.
	
	See SystemDictionary>>#obsoleteClasses"

	environments do: [ :environment | environment addObsoleteClassesTo: anOrderedCollection ]! !

!SimpleEnvironmentManager methodsFor: 'housekeeping' stamp: 'KLG 10/6/2020 14:47:08'!
cleanOutUndeclared
	"Clean undeclared stuff in every environment.
	
	See See SystemDictionary>>#cleanOutUndeclared"
	
	environments do: [ :environment | environment cleanOutUndeclared ]! !

!SimpleEnvironmentManager methodsFor: 'retrieving' stamp: 'KLG 10/6/2020 14:47:08'!
addToPoolUsers: aDictionary
	"Add references to shared pools to aDictionary.
	
	See SystemDictionary>>#poolUsers"

	environments do: [ :environment | environment addToPoolUsers: aDictionary]! !

!SimpleEnvironmentManager methodsFor: 'retrieving' stamp: 'KLG 10/6/2020 14:47:08'!
allBehaviorsDo: aBlock 
	"Evaluate the argument, aBlock, for each kind of Behavior in the system 
	(that is, Object and its subclasses).
	
	See SystemDictionary>>#allBehaviorsDo:"

	environments do: [ :environment | environment allBehaviorsDo: aBlock ]! !

!SimpleEnvironmentManager methodsFor: 'scanning' stamp: 'KLG 10/19/2020 14:50:31'!
scanClassDenotation: aClassDenotation
	"Scan aClassDenotation, answer nil if the denotation is not handled by this environment implementation."
	
	
	aClassDenotation isArray ifFalse: [ ^ nil ].
	aClassDenotation size = 5 ifFalse: [ ^ nil ].
	aClassDenotation allSatisfy: [ :element | element isSymbol ] :: ifFalse: [ ^ nil ].
	aClassDenotation first == self class singleInstanceGlobalName ifFalse: [ ^ nil ].
	aClassDenotation second == #>> ifFalse: [ ^ nil ].
	aClassDenotation fourth == #>> ifFalse: [ ^ nil ].
	^ aClassDenotation fifth forEnvironment: (self environment: aClassDenotation third)! !

!SimpleEnvironmentManager class methodsFor: 'instance creation' stamp: 'KLG 10/6/2020 14:58:11'!
environment: aNameSymbol
	"Answer the environment denoted by aNameSymbol"

	^ singleInstance environment: aNameSymbol! !

!SimpleEnvironmentManager class methodsFor: 'instance creation' stamp: 'KLG 10/8/2020 16:59:36'!
environmentClass
	"Answer the environment manager class."
	
	^ SimpleEnvironment! !

!SimpleEnvironmentManager class methodsFor: 'naming' stamp: 'KLG 10/6/2020 14:57:30'!
environmentImplementationName
	"Answer the name to use to install this class in the environment implementations dictionary.
	
	Answer nil if you do not want yor implementation be installed."

	^ self name! !

!SimpleEnvironmentManager class methodsFor: 'class initialization' stamp: 'KLG 10/21/2020 16:45:16'!
initialize
	"Create the single instance and install it in the environment implementations dictionary."

	super initialize.
	singleInstance _ self new.
	self environmentImplementationName ifNotNil: [ :implementationName |
		EnvironmentImplementations
			at: implementationName
			put: singleInstance.
		self singleInstanceGlobalName ifNotNil: [ :singleInstanceName |
			Smalltalk at: singleInstanceName asSymbol put: singleInstance ] ].! !

!SimpleEnvironmentManager class methodsFor: 'denoting objects' stamp: 'KLG 10/8/2020 16:45:11'!
singleInstanceGlobalName
	"Answer the name at which our single instance is put into Smalltalk.

	Answer nil if you do not want the single instance installed in Smalltalk.

	This enables nice syntax such as:
	SimpleEnvironments>>#MyFunnyEnnvironment>>#MyFunnyClass
		or even
	SimpleEnvironments>>#MyFunnyEnnvironment>>#MyFunnyClass>>#mySillySelector
	"
	
	^ #SimpleEnvironments! !

!SimpleEnvironmentManager class methodsFor: 'conversion' stamp: 'KLG 10/6/2020 17:27:50'!
convertToEnvironmentName: anObject
	"Convert an object to a suitable envernment specification.
	
	May be redefined in subclasses."

	^ anObject asSymbol! !

!Class methodsFor: '*SimpleEnvironments' stamp: 'KLG 10/17/2020 16:39:18'!
subclass: t instanceVariableNames: f classVariableNames: d poolDictionaries: s category: cat iSE: e
	"This is the initialization message for creating a new class as a 
	subclass of an existing class (the receiver) in an evironment.
	
	This is just an very short version to ease typing"

	^ self
		subclass: t
		instanceVariableNames: f
		classVariableNames: d
		poolDictionaries: s
		category: cat
		inSimpleEnvironment: e.! !

!Class methodsFor: '*SimpleEnvironments' stamp: 'KLG 10/8/2020 11:30:42'!
subclass: t 
	instanceVariableNames: f 
	classVariableNames: d 
	poolDictionaries: s 
	category: cat 
	inSimpleEnvironment: e
	"This is the initialization message for creating a new class as a 
	subclass of an existing class (the receiver) in an evironment."
	
	| answer |
	answer _ SimpleEnvironmentClassBuilder new
		superclass: self
		subclass: t
		instanceVariableNames: f
		classVariableNames: d
		poolDictionaries: s
		category: cat
		environment: e.
		
	Smalltalk
		logChange: answer definition 
		preamble: answer definitionPreamble.
	^answer
! !
SimpleEnvironmentManager initialize!
