'From Cuis 5.0 [latest update: #4439] on 5 November 2020 at 7:40:15 pm'!
'Description Environments that support imports from other environments or simple environments.'!
!provides: 'Environments' 1 11!
!requires: 'SimpleEnvironments' 1 25 nil!
SystemOrganization addCategory: 'Environments'!


!classDefinition: (SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) category: 'Environments'!
SimpleEnvironmentMetaclass subclass: #EnvironmentMetaclass
	instanceVariableNames: 'importSpecifications bindingsDirty importedBindings'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Environments'
	inSimpleEnvironment: #Environments!
!classDefinition: (SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) class category: 'Environments'!
(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) class
	instanceVariableNames: ''!

!classDefinition: (SimpleEnvironments>>#Environments>>#EnvironmentClassBuilder) category: 'Environments'!
SimpleEnvironmentClassBuilder subclass: #EnvironmentClassBuilder
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Environments'
	inSimpleEnvironment: #Environments!
!classDefinition: (SimpleEnvironments>>#Environments>>#EnvironmentClassBuilder) class category: 'Environments'!
(SimpleEnvironments>>#Environments>>#EnvironmentClassBuilder) class
	instanceVariableNames: ''!

!classDefinition: (SimpleEnvironments>>#Environments>>#Environment) category: 'Environments'!
SimpleEnvironment subclass: #Environment
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Environments'
	inSimpleEnvironment: #Environments!
!classDefinition: (SimpleEnvironments>>#Environments>>#Environment) class category: 'Environments'!
(SimpleEnvironments>>#Environments>>#Environment) class
	instanceVariableNames: ''!

!classDefinition: (SimpleEnvironments>>#Environments>>#EnvironmentManager) category: 'Environments'!
SimpleEnvironmentManager subclass: #EnvironmentManager
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Environments'
	inSimpleEnvironment: #Environments!
!classDefinition: (SimpleEnvironments>>#Environments>>#EnvironmentManager) class category: 'Environments'!
(SimpleEnvironments>>#Environments>>#EnvironmentManager) class
	instanceVariableNames: ''!

!classDefinition: (SimpleEnvironments>>#Environments>>#ImportSpecification) category: 'Environments'!
Object subclass: #ImportSpecification
	instanceVariableNames: 'environment externalSymbol internalSymbol'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Environments'
	inSimpleEnvironment: #Environments!
!classDefinition: (SimpleEnvironments>>#Environments>>#ImportSpecification) class category: 'Environments'!
(SimpleEnvironments>>#Environments>>#ImportSpecification) class
	instanceVariableNames: ''!


!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) commentStamp: '<historical>' prior: 0!
I am the metaclass for environments.!

!(SimpleEnvironments>>#Environments>>#EnvironmentClassBuilder) commentStamp: '<historical>' prior: 0!
I am the class builder for environments with imports.

Currently we sole purpose is to direct the framework to use right metaclass.!

!(SimpleEnvironments>>#Environments>>#Environment) commentStamp: '<historical>' prior: 0!
I am an environment with imports.!

!(SimpleEnvironments>>#Environments>>#EnvironmentManager) commentStamp: '<historical>' prior: 0!
I am the environment manager for normal environments.!

!(SimpleEnvironments>>#Environments>>#ImportSpecification) commentStamp: '<historical>' prior: 0!
I specified an imported symbol.!

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) methodsFor: 'compiling' stamp: 'KLG 11/5/2020 08:51:41'!
forNonLocalBindingEnvironmentsDo: aBlock
	"Evaluate aBlock for all non local environments."

	bindingsDirty ifTrue: [ self buildImportedBindings ].
	aBlock value: importedBindings.
	super forNonLocalBindingEnvironmentsDo: aBlock! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) methodsFor: 'fileIn/Out' stamp: 'KLG 11/3/2020 18:31:08'!
printAdditionalDefinitionOn: aStream
	"Print additional defintion information on aStream.

	Print the environment to use."

	aStream
		newLine;
		tab;
		nextPutAll: 'inEnvironment: ';
		store: environment name.
	importSpecifications do: [ :importSpecification |
		aStream 	nextPutAll: ' ::'; lf; tab; tab.
		importSpecification printAdditionalDefinitionOn: aStream ]
		! !

!(SimpleEnvironments>>#Environments>>#ImportSpecification) methodsFor: 'fileIn/Out' stamp: 'KLG 11/3/2020 18:33:24'!
printAdditionalDefinitionOn: aStream
	"Print the import specification of aStream."

	aStream nextPutAll: 'import: '.
	self externalSymbol storeOn: aStream.
	aStream nextPutAll: ' from: '.
	self environment isKindOf: self environmentRootClass ::
		ifTrue: [ self environment name storeOn: aStream ]
		ifFalse: [ 	self environment printOn: aStream ].
	self externalSymbol = self internalSymbol ifFalse: [
		aStream nextPutAll: ' as: '.
		self internalSymbol storeOn: aStream ]! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) methodsFor: 'importing' stamp: 'KLG 11/5/2020 09:29:56'!
buildImportedBindings
	"Build the imported bindings."

	importSpecifications do: [ :importSpecification | | internalSymbol externalValue |
		externalValue _ importSpecification environment at: importSpecification externalSymbol.
		importedBindings
			at: (internalSymbol _ importSpecification internalSymbol)
			ifPresent: [ :ignoredValue |
				importedBindings associationAt: internalSymbol :: value: externalValue ]
			ifAbsent: [
				importedBindings
					at: internalSymbol
					put: externalValue ]].
	importedBindings associationsDo: [ :assoc |
		importSpecifications includesKey: assoc key :: ifFalse: [
			assoc value: nil ] ].
	bindingsDirty _ false! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) methodsFor: 'importing' stamp: 'KLG 11/3/2020 16:28:55'!
import: anExternalSymbol from: anEnvironment
	"Import anExternalSymbol form anEnvironment"

	self import: anExternalSymbol from: anEnvironment as: anExternalSymbol! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) methodsFor: 'importing' stamp: 'KLG 11/5/2020 08:53:01'!
import: anExternalSymbol from: anEnvironment as: anInternalSymbol
	"Import anExternalSymbol form anEnvironment as anInternalSymbol"

	| environmentToImportFrom |
	bindingsDirty _ true.
	environmentToImportFrom _ anEnvironment isSymbol
		ifTrue: [ self environmentManagerClass environment: anEnvironment ]
		ifFalse: [ anEnvironment ].
	importSpecifications 
		at: anInternalSymbol
		put:	(self importSpecificationClass
				for: environmentToImportFrom
				externalSymbol: anExternalSymbol
				internalSymbol: anInternalSymbol)! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) methodsFor: 'importing' stamp: 'KLG 11/5/2020 09:37:54'!
removeImport: anInternalSymbol
	"Remove an import."

	importSpecifications removeKey: anInternalSymbol ifAbsent: [ ^self ].
	bindingsDirty _ true! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) methodsFor: 'class builder interface' stamp: 'KLG 11/5/2020 08:54:03'!
configureFromClassBuilder: aClassBuilder
	"Configure this environment from aClassBuilder.
	
	Actually we retrieve nothing from aClassBuilder,
	but we use this method to set our other instance variables."

	super configureFromClassBuilder: aClassBuilder.
	bindingsDirty _ true! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) methodsFor: 'instance creation' stamp: 'KLG 11/3/2020 16:27:20'!
importSpecificationClass
	"Answer the class of the import specifications."

	^ self class importSpecificationClass! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) methodsFor: 'converting' stamp: 'KLG 11/3/2020 18:21:12'!
environmentManagerClass
	"Answer the environment manager class."

	^ self class environmentManagerClass! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) methodsFor: 'initialization' stamp: 'KLG 11/5/2020 08:40:29'!
initialize
	"Initialize our instance variables (what else, Dude?"

	super initialize.
	bindingsDirty _ true.
	importSpecifications _ GlobalBindingIdentityDictionary new.
	importedBindings _ GlobalBindingIdentityDictionary new
	! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) class methodsFor: 'instance creation' stamp: 'KLG 11/3/2020 16:22:56'!
importSpecificationClass
	"Answer the class of the import specifications."

	^ ImportSpecification! !

!(SimpleEnvironments>>#Environments>>#EnvironmentMetaclass) class methodsFor: 'converting' stamp: 'KLG 11/4/2020 12:31:29'!
environmentManagerClass
	"Answer the environment manager class."

	^ EnvironmentManager! !

!(SimpleEnvironments>>#Environments>>#EnvironmentClassBuilder) class methodsFor: 'instance creation' stamp: 'KLG 11/4/2020 16:39:00'!
environmentManagerClass
	"Answer the environment manager class."

	"XXX: Don't know wy direct reference does not work"
	^ `SimpleEnvironments>>#Environments>>#EnvironmentManager`! !

!(SimpleEnvironments>>#Environments>>#EnvironmentClassBuilder) class methodsFor: 'instance creation' stamp: 'KLG 11/3/2020 15:37:45'!
metaclassClass
	"Answer the proper meta class for me."

	^ EnvironmentMetaclass! !

!(SimpleEnvironments>>#Environments>>#Environment) class methodsFor: 'manager' stamp: 'KLG 11/4/2020 16:28:43'!
managerClass
	"Answer the environment manager class."

	^ EnvironmentManager ! !

!(SimpleEnvironments>>#Environments>>#EnvironmentManager) class methodsFor: 'instance creation' stamp: 'KLG 11/4/2020 12:27:49'!
environmentClass
	"Answer the environment manager class."
	
	^ Environment! !

!(SimpleEnvironments>>#Environments>>#EnvironmentManager) class methodsFor: 'denoting objects' stamp: 'KLG 11/4/2020 08:19:21'!
singleInstanceGlobalName
	"Answer the name at which our single instance is put into Smalltalk.

	Answer nil if you do not want the single instance installed in Smalltalk.	"
	
	^ #Environments! !

!(SimpleEnvironments>>#Environments>>#EnvironmentManager) class methodsFor: 'initialization' stamp: 'KLG 11/5/2020 19:14:35'!
initialize
	"Trigger class initialization upon install."

	super initialize! !

!(SimpleEnvironments>>#Environments>>#ImportSpecification) methodsFor: 'accessing' stamp: 'KLG 11/3/2020 16:20:07'!
environment
	"Answer the value of environment"

	self assert: environment notNil.
	^ environment! !

!(SimpleEnvironments>>#Environments>>#ImportSpecification) methodsFor: 'accessing' stamp: 'KLG 11/3/2020 16:20:07'!
environment: anObject
	"Set the value of environment"

	self assert: environment isNil.
	environment _ anObject! !

!(SimpleEnvironments>>#Environments>>#ImportSpecification) methodsFor: 'accessing' stamp: 'KLG 11/3/2020 16:20:07'!
externalSymbol
	"Answer the value of externalSymbol"

	self assert: externalSymbol notNil.
	^ externalSymbol! !

!(SimpleEnvironments>>#Environments>>#ImportSpecification) methodsFor: 'accessing' stamp: 'KLG 11/3/2020 16:20:07'!
externalSymbol: anObject
	"Set the value of externalSymbol"

	self assert: externalSymbol isNil.
	externalSymbol _ anObject! !

!(SimpleEnvironments>>#Environments>>#ImportSpecification) methodsFor: 'accessing' stamp: 'KLG 11/3/2020 16:20:07'!
internalSymbol
	"Answer the value of internalSymbol"

	self assert: internalSymbol notNil.
	^ internalSymbol! !

!(SimpleEnvironments>>#Environments>>#ImportSpecification) methodsFor: 'accessing' stamp: 'KLG 11/3/2020 16:20:07'!
internalSymbol: anObject
	"Set the value of internalSymbol"

	self assert: internalSymbol isNil.
	internalSymbol _ anObject! !

!(SimpleEnvironments>>#Environments>>#ImportSpecification) methodsFor: 'class membership' stamp: 'KLG 11/3/2020 18:26:49'!
environmentRootClass
	"Answer the environment class."
	
	^ self class environmentRootClass! !

!(SimpleEnvironments>>#Environments>>#ImportSpecification) class methodsFor: 'instance creation' stamp: 'KLG 11/3/2020 16:21:00'!
for: anEnvironment externalSymbol: anExternalSymbol internalSymbol: anInternalSymbol
	"Create a new import specification."

	^ self basicNew
		environment: anEnvironment;
		externalSymbol: anExternalSymbol;
		internalSymbol: anInternalSymbol;
		yourself! !

!(SimpleEnvironments>>#Environments>>#ImportSpecification) class methodsFor: 'class membership' stamp: 'KLG 11/4/2020 14:54:05'!
environmentRootClass
	"Answer the environment class."
	
	^ Environment! !

!Class methodsFor: '*Environments' stamp: 'KLG 11/4/2020 14:59:29'!
from: anEvironment import: anExternalSymbol 
	"Import a symbol.
	
	I can't remember the right kewword order"

	self import: anExternalSymbol from: anEvironment! !

!Class methodsFor: '*Environments' stamp: 'KLG 11/4/2020 14:59:51'!
from: anEvironment import: anExternalSymbol as: anInternalSymbol
	"Import a symbol.
	
	I can't remember the right kewword order"

	self import: anExternalSymbol from: anEvironment as: anInternalSymbol! !

!Class methodsFor: '*Environments' stamp: 'KLG 11/3/2020 17:09:08'!
import: anExternalSymbol from: anEvironment
	"Import a symbol."

	self class import: anExternalSymbol from: anEvironment! !

!Class methodsFor: '*Environments' stamp: 'KLG 11/3/2020 17:04:21'!
import: anExternalSymbol from: anEvironment as: anInternalSymbol
	"Import a symbol."

	self class import: anExternalSymbol from: anEvironment as: anInternalSymbol! !

!Class methodsFor: '*Environments' stamp: 'KLG 11/5/2020 09:38:29'!
removeImport: anInternalSymbol
	"Remove an import."
	
	self class removeImport: anInternalSymbol! !

!Class methodsFor: '*Environments' stamp: 'KLG 11/3/2020 16:41:04'!
subclass: t instanceVariableNames: f classVariableNames: d poolDictionaries: s category: cat iE: e
	"This is the initialization message for creating a new class as a 
	subclass of an existing class (the receiver) in an evironment.
	
	This is just an very short version to ease typing"

	^ self
		subclass: t
		instanceVariableNames: f
		classVariableNames: d
		poolDictionaries: s
		category: cat
		inEnvironment: e.! !

!Class methodsFor: '*Environments' stamp: 'KLG 11/3/2020 16:40:37'!
subclass: t 
	instanceVariableNames: f 
	classVariableNames: d 
	poolDictionaries: s 
	category: cat 
	inEnvironment: e
	"This is the initialization message for creating a new class as a 
	subclass of an existing class (the receiver) in an evironment."
	
	| answer |
	answer _ (SimpleEnvironments>>#Environments>>#EnvironmentClassBuilder) new
		superclass: self
		subclass: t
		instanceVariableNames: f
		classVariableNames: d
		poolDictionaries: s
		category: cat
		environment: e.
		
	Smalltalk
		logChange: answer definition 
		preamble: answer definitionPreamble.
	^ answer
! !

!Class methodsFor: '*Environments' stamp: 'KLG 11/5/2020 18:01:57'!
subclass: t instanceVariableNames: f classVariableNames: d poolDictionaries: s iE: e
	"This is the initialization message for creating a new class as a 
	subclass of an existing class (the receiver) in an evironment.
	
	This is just an very short version to ease typing"

	^ self
		subclass: t
		instanceVariableNames: f
		classVariableNames: d
		poolDictionaries: s
		category: e
		inEnvironment: e.! !

!Class methodsFor: '*Environments' stamp: 'KLG 11/5/2020 18:02:21'!
subclass: t instanceVariableNames: f classVariableNames: d poolDictionaries: s inEEnvironment: e
	"This is the initialization message for creating a new class as a 
	subclass of an existing class (the receiver) in an evironment.
	
	This is just an very short version to ease typing"

	^ self
		subclass: t
		instanceVariableNames: f
		classVariableNames: d
		poolDictionaries: s
		category: e
		inEnvironment: e.! !
(SimpleEnvironments>>#Environments>>#EnvironmentManager) initialize!
