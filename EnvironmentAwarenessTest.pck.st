'From Cuis 5.0 [latest update: #4438] on 11 November 2020 at 7:18:21 pm'!
'Description '!
!provides: 'EnvironmentAwarenessTest' 1 10!
!requires: 'EnvironmentAwareness' 1 21 nil!
SystemOrganization addCategory: 'EnvironmentAwarenessTest'!


!classDefinition: #EAAbstractTestCase category: 'EnvironmentAwarenessTest'!
TestCase subclass: #EAAbstractTestCase
	instanceVariableNames: 'environment1 environment2 string1 string2 symbol1 symbol2'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentAwarenessTest'!
!classDefinition: 'EAAbstractTestCase class' category: 'EnvironmentAwarenessTest'!
EAAbstractTestCase class
	instanceVariableNames: ''!

!classDefinition: #EAIdentityDictionaryTest category: 'EnvironmentAwarenessTest'!
EAAbstractTestCase subclass: #EAIdentityDictionaryTest
	instanceVariableNames: 'id1 id2 ebid1 ebid2 value1 value2 gbid1 gbid2'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentAwarenessTest'!
!classDefinition: 'EAIdentityDictionaryTest class' category: 'EnvironmentAwarenessTest'!
EAIdentityDictionaryTest class
	instanceVariableNames: ''!

!classDefinition: #EASymbolTest category: 'EnvironmentAwarenessTest'!
EAAbstractTestCase subclass: #EASymbolTest
	instanceVariableNames: 'symbol1in1 symbol1in2 symbol2in1 symbol2in2'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentAwarenessTest'!
!classDefinition: 'EASymbolTest class' category: 'EnvironmentAwarenessTest'!
EASymbolTest class
	instanceVariableNames: ''!


!EAAbstractTestCase methodsFor: 'setUp/tearDown' stamp: 'KLG 10/10/2020 20:11:05'!
setUp
	"Set up two environments and a symbol"

	| nowHashString |
	super setUp.
	nowHashString _ DateAndTime now hash asString.
	string1 _ 'S1', nowHashString.
	string2 _ 'S2', nowHashString.
	symbol1 _ string1 asSymbol.
	symbol2 _ string2 asSymbol.	
	environment1 _ Object new.
	environment2 _ Object new
! !

!EAAbstractTestCase methodsFor: 'setUp/tearDown' stamp: 'KLG 10/10/2020 21:12:06'!
tearDown
	"Remove all subclasses of AbstractSymbolInEnvironment"

	super tearDown.
	AbstractSymbolInEnvironment removeAllSubclasses! !

!EAAbstractTestCase methodsFor: 'testing' stamp: 'KLG 10/11/2020 13:34:21'!
makeRandomSymbol

	^ Random 
		withDefaultDo: [ :random | random nextInteger: 1000 ] :: asString asSymbol! !

!EAIdentityDictionaryTest methodsFor: 'setUp/tearDown' stamp: 'KLG 10/11/2020 15:27:05'!
setUp
	"Setup some dictionaries."

	super setUp.
	id1 _ IdentityDictionary new.
	id2 _ IdentityDictionary new.
	ebid1 _ EnvironmentBindingIdentityDictionary forEnvironment: environment1.
	ebid2 _ EnvironmentBindingIdentityDictionary forEnvironment: environment2.
	gbid1 _ GlobalBindingIdentityDictionary new.
	gbid2 _ GlobalBindingIdentityDictionary new.
	value1 _ Object new.
	value2 _ Object new! !

!EAIdentityDictionaryTest methodsFor: 'testing' stamp: 'KLG 10/11/2020 15:28:02'!
testAtPut
	"Test putting and retrieving values from dictionaries."

	id1 at: symbol1 put: value1.
	self assert: value1 equals: (id1 at: symbol1).
	ebid1 at: symbol1 asEnvironmentAwareSymbol put: value1.
	self assert: value1 equals: (id1 at: symbol1).
	self assert: value1 equals: (ebid1 at: symbol1).
	self assert: value1 equals: (ebid1 at: (symbol1 forEnvironment: environment1)).
	gbid1 at: symbol1 asEnvironmentAwareSymbol put: value1.
	self assert: value1 equals: (id1 at: symbol1).
	self assert: value1 equals: (gbid1 at: symbol1).
	self assert: value1 equals: (gbid1 at: (symbol1 forEnvironment: environment1)).! !

!EAIdentityDictionaryTest methodsFor: 'testing' stamp: 'KLG 10/11/2020 13:34:10'!
testAtPutOfAnOrdinarySymbol
	"Test putting an ordinary symbol"

	| os |
	os _ self makeRandomSymbol.
	id1 at: os put: value1.
	self assert: value1 equals: (id1 at: os).
	ebid1 at: os put: value1.
	self assert: value1 equals: (ebid1 at: os).
	self assert: (ebid1 printString isKindOf: String).
	ebid1 print.
	self assert: value1 equals: (ebid1 at: os asEnvironmentAwareSymbol).
	self assert: (ebid1 printString isKindOf: String).
	ebid1 print.
! !

!EAIdentityDictionaryTest methodsFor: 'testing' stamp: 'KLG 10/13/2020 10:21:25'!
testDeclare
	"Test the declare stuff, becuase we modify an association."

	id1 at: symbol1 put: value1.
	self assert: value1 equals: (id1 at: symbol1).
	ebid1 declare: symbol1 from: id1.
	self should: [ id1 at: symbol1 ] raise: Error.
	self assert: value1 equals: (ebid1 at: symbol1)! !

!EAIdentityDictionaryTest methodsFor: 'testing' stamp: 'KLG 10/13/2020 10:20:15'!
testRemove
	"Test removing keys."

	id1 at: symbol1 put: value1.
	self assert: value1 equals: (id1 at: symbol1).
	id1 removeKey: symbol1.
	self shouldnt: [ id1 includesKey: symbol1 ].
	ebid1 at: symbol1 put: value1.
	self assert: value1 equals: (ebid1 at: symbol1).
	ebid1 removeKey: symbol1.
	self shouldnt: [ ebid1 includesKey: symbol1 ].
! !

!EASymbolTest methodsFor: 'setUp/tearDown' stamp: 'KLG 10/10/2020 20:11:51'!
setUp
	"Set up two environments and a symbol"
	
	super setUp.
	symbol1in1 _ symbol1 forEnvironment: environment1.
	symbol1in2 _ symbol1 forEnvironment: environment2.
	symbol2in1 _ symbol2 forEnvironment: environment1.
	symbol2in2 _ symbol2 forEnvironment: environment2
! !

!EASymbolTest methodsFor: 'testing' stamp: 'KLG 11/11/2020 14:22:12'!
testConversionFromOneEnvironmentToAnother
	"Make sure we can convert from a symbol in one environment directly into a symbol in an other one."
	 
	| environment3 symbol1in3 |
	environment3 _ Object new.
	symbol1in3 _ symbol1in1 forEnvironment: environment3.
	self assert: symbol1in3 equals: (symbol1in2 forEnvironment: environment3).
	self assert: symbol1 ~= symbol1in3.
	self assert: symbol1in3 == (symbol1in2 forEnvironment: environment3).
	self assert: symbol1 ~~ symbol1in3.! !

!EASymbolTest methodsFor: 'testing' stamp: 'KLG 11/11/2020 18:10:14'!
testConversionFromOrdinaryObjectsAndBack
	"Make sure we can convert any thing, most objects are converted to themselves."
	 
	| someObject |
	self assert:
		(someObject _ Object new) == (someObject forEnvironmentIfAware: environment1).
	self assert:
		(someObject _ String new) == (someObject forEnvironmentIfAware: environment1).
	self assert:
		(someObject _ Smalltalk) ==  (someObject forEnvironmentIfAware: environment1).
	self assert: 
		(someObject _ self makeRandomSymbol ) ~~
		(someObject forEnvironmentIfAware: environment1).
	self assert: 
		someObject == 		(someObject
						forEnvironmentIfAware: environment1) forGlobalNamespace.
	someObject _ self makeRandomSymbol.
	self assert: (someObject forEnvironment: environment1) == (someObject forEnvironment: environment1)! !

!EASymbolTest methodsFor: 'testing' stamp: 'KLG 10/10/2020 14:47:48'!
testEnvironments
	"Test environments in the symbol(s| classes)."

	 
	self assert: symbol1in1 environment == environment1.
	self assert: symbol2in1 environment == environment1.
	self assert: symbol1in2 environment == environment2.
	self assert: symbol2in2 environment == environment2
! !

!EASymbolTest methodsFor: 'testing' stamp: 'KLG 11/11/2020 11:46:54'!
testEquality
	"Test the symbol equality."

	 
	self assert: symbol1 ~= symbol2.
	self assert: symbol1 ~= symbol1in1.
	self assert: symbol1 ~= symbol1in2.
	self assert: symbol2 ~= symbol2in1.
	self assert: symbol2 ~= symbol2in2.
	self assert: symbol1in1 ~= symbol1in2.
	self assert: symbol2in1 ~= symbol2in2.
	self assert: symbol1in1 ~= symbol2in1.
	self assert: symbol1in2 ~= symbol2in2! !

!EASymbolTest methodsFor: 'testing' stamp: 'KLG 10/10/2020 15:51:34'!
testIdentity
	"Test the symbol identity."

	 
	self assert: symbol1 ~~ symbol2.
	self assert: symbol1in1 ~~ symbol1in2.
	self assert: symbol2in1 ~~ symbol2in2.
	self assert: symbol1 ~~ symbol1in1.
	self assert: symbol1 ~~ symbol1in2.
	self assert: symbol2 ~~ symbol2in1.
	self assert: symbol2 ~~ symbol2in2.
	self assert: symbol1 == string1 asSymbol.
	self assert: symbol2 == string2 asSymbol.! !

!EASymbolTest methodsFor: 'testing' stamp: 'KLG 10/10/2020 15:52:14'!
testKinds
	"Test the symbols."

	 
	self assert: symbol1 class == EnvironmentAwareSymbol.
	self assert: symbol2 class == EnvironmentAwareSymbol.
	self assert: string1 asSymbol class == EnvironmentAwareSymbol.
	self assert: string2 asSymbol class == EnvironmentAwareSymbol.
	self assert: (symbol1in1 isKindOf: AbstractSymbolInEnvironment).
	self assert: (symbol1in2 isKindOf: AbstractSymbolInEnvironment).
	self assert: (symbol2in1 isKindOf: AbstractSymbolInEnvironment).
	self assert: (symbol2in2 isKindOf: AbstractSymbolInEnvironment).
	self assert: symbol1in1 class == symbol2in1 class.
	self assert: symbol1in2 class == symbol2in2 class.
	self assert: symbol1in1 class ~~ symbol1in2 class.
	self assert: symbol2in1 class ~~ symbol2in2 class.! !
