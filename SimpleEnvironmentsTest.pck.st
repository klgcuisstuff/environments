'From Cuis 5.0 [latest update: #4392] on 14 October 2020 at 2:45:21 pm'!
'Description Tests for the simple environments package'!
!provides: 'SimpleEnvironmentsTest' 1 3!
!requires: 'SimpleEnvironments' 1 16 nil!
SystemOrganization addCategory: 'SimpleEnvironmentsTest'!


!classDefinition: #SimpleSimpleEnvironmentTestCase category: 'SimpleEnvironmentsTest'!
TestCase subclass: #SimpleSimpleEnvironmentTestCase
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SimpleEnvironmentsTest'!
!classDefinition: 'SimpleSimpleEnvironmentTestCase class' category: 'SimpleEnvironmentsTest'!
SimpleSimpleEnvironmentTestCase class
	instanceVariableNames: ''!


!SimpleSimpleEnvironmentTestCase commentStamp: '<historical>' prior: 0!
I am a very simple test case for simple environments.!

!SimpleSimpleEnvironmentTestCase methodsFor: 'configuration' stamp: 'KLG 10/14/2020 14:40:06'!
classesForTestNames
	"Answer the names of the subclasses to use."

	^ #(TC1 TC2 TC3)! !

!SimpleSimpleEnvironmentTestCase methodsFor: 'configuration' stamp: 'KLG 10/14/2020 10:45:07'!
environmentName
	"Answer the name of the test environment."

	^ #TE1! !

!SimpleSimpleEnvironmentTestCase methodsFor: 'configuration' stamp: 'KLG 10/14/2020 10:50:19'!
shouldRemoveTestClasses
	"Answer true if we should remove the test classes after each test."

	^ false! !

!SimpleSimpleEnvironmentTestCase methodsFor: 'accessing' stamp: 'KLG 10/14/2020 14:39:32'!
classForTestNamed: aNameSymbol
	"Answer the test class denoted by a name symbol."

	^ SimpleEnvironments >> self environmentName >> aNameSymbol! !

!SimpleSimpleEnvironmentTestCase methodsFor: 'testing' stamp: 'KLG 10/14/2020 14:40:06'!
testAccessability
	"Test the accessability of classes."

	self classesForTestNames do: [ :subclassName |
		self shouldnt: [ Smalltalk includesKey: subclassName ].
		self 
			assert: subclassName
			equals:
				(SimpleEnvironments >> 
					self environmentName >> 
						subclassName :: name) ]! !

!SimpleSimpleEnvironmentTestCase methodsFor: 'enumeration' stamp: 'KLG 10/14/2020 14:40:26'!
classesForTestDo: aBlock
	"Evaluate aBlock fro all test classes."
	
	self classesForTestNames do: [ :testClassName |
		aBlock value: (self classForTestNamed: testClassName) ]
	! !

!SimpleSimpleEnvironmentTestCase methodsFor: 'enumeration' stamp: 'KLG 10/14/2020 14:40:49'!
classesForTestReverseDo: aBlock
	"Evaluate aBlock for all test classes in reverse order."
	
	self classesForTestNames reverseDo: [ :testClassName |
		aBlock value: (self classForTestNamed: testClassName) ]
	! !

!SimpleSimpleEnvironmentTestCase methodsFor: 'setUp/tearDown' stamp: 'KLG 10/14/2020 14:39:32'!
addTestMethod: someSourceCode to: aClassOrClassName

	"Add a test method to a class."
	
	| class |
	class _ aClassOrClassName isBehavior 
		ifTrue: [ aClassOrClassName ]
		ifFalse: [ self classForTestNamed: aClassOrClassName ].
	class compile: someSourceCode classified: 'test code'  notifying: nil! !

!SimpleSimpleEnvironmentTestCase methodsFor: 'setUp/tearDown' stamp: 'KLG 10/14/2020 14:40:06'!
setUp
	"Set up a simple hierarchy of 3 classes."

	| superclass |
	super setUp.
	superclass _ Object.
	self classesForTestNames do: [ :testClassName |
		superclass _
			superclass subclass: testClassName 
				instanceVariableNames:  '' 
				classVariableNames: ''
				poolDictionaries: '' 
				category: 'SimpleEnvironmentsTest - TestClasses'
				inSimpleEnvironment: self environmentName ].
	self classesForTestNames do: [ :testClassName |
		self 
			addTestMethod:  
'm1

	^ ''m1 ', testClassName, '''
'
			to: testClassName ]! !

!SimpleSimpleEnvironmentTestCase methodsFor: 'setUp/tearDown' stamp: 'KLG 10/14/2020 14:40:49'!
tearDown
	"Set up a simple hierarchy of 3 classes."

	self shouldRemoveTestClasses ifTrue: [
		self classesForTestReverseDo: [ :testClass | testClass removeFromSystem ] ]! !
