'From Cuis 5.0 [latest update: #4438] on 11 November 2020 at 7:29:06 pm'!
'Description I contain subclasses of Symbol and IdentityDictionary that aware the environment a symbol belongs to.'!
!provides: 'EnvironmentAwareness' 1 22!
!requires: 'Cuis-Base' 50 4392 nil!
SystemOrganization addCategory: 'EnvironmentAwareness'!


!classDefinition: #AbstractSymbolInEnvironment category: 'EnvironmentAwareness'!
Symbol variableByteSubclass: #AbstractSymbolInEnvironment
	instanceVariableNames: ''
	classVariableNames: 'SubclassesByEnvironment'
	poolDictionaries: ''
	category: 'EnvironmentAwareness'!
!classDefinition: 'AbstractSymbolInEnvironment class' category: 'EnvironmentAwareness'!
AbstractSymbolInEnvironment class
	instanceVariableNames: 'environment symbolTable newSymbols'!

!classDefinition: #EnvironmentAwareSymbol category: 'EnvironmentAwareness'!
Symbol variableByteSubclass: #EnvironmentAwareSymbol
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentAwareness'!
!classDefinition: 'EnvironmentAwareSymbol class' category: 'EnvironmentAwareness'!
EnvironmentAwareSymbol class
	instanceVariableNames: ''!

!classDefinition: #EnvironmentBindingIdentityDictionary category: 'EnvironmentAwareness'!
IdentityDictionary subclass: #EnvironmentBindingIdentityDictionary
	instanceVariableNames: 'environment'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentAwareness'!
!classDefinition: 'EnvironmentBindingIdentityDictionary class' category: 'EnvironmentAwareness'!
EnvironmentBindingIdentityDictionary class
	instanceVariableNames: ''!

!classDefinition: #GlobalBindingIdentityDictionary category: 'EnvironmentAwareness'!
IdentityDictionary subclass: #GlobalBindingIdentityDictionary
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentAwareness'!
!classDefinition: 'GlobalBindingIdentityDictionary class' category: 'EnvironmentAwareness'!
GlobalBindingIdentityDictionary class
	instanceVariableNames: ''!

!classDefinition: #WeakSymbolInEnvironmentSet category: 'EnvironmentAwareness'!
WeakSet subclass: #WeakSymbolInEnvironmentSet
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentAwareness'!
!classDefinition: 'WeakSymbolInEnvironmentSet class' category: 'EnvironmentAwareness'!
WeakSymbolInEnvironmentSet class
	instanceVariableNames: ''!


!AbstractSymbolInEnvironment commentStamp: '<historical>' prior: 0!
I am the abstract super class of all environment specific Symbol sublasses. !

!EnvironmentAwareSymbol commentStamp: '<historical>' prior: 0!
I am the class of all classes that are environment aware.

However there exists a sub class of AbstractSymbolInEnvironment for each environment.!

!EnvironmentBindingIdentityDictionary commentStamp: '<historical>' prior: 0!
I am an IdentityDictionary that binds its keys to the environment
specific symbol namespace.

Needless to say that this is only done for keys that are symbols.!

!GlobalBindingIdentityDictionary commentStamp: '<historical>' prior: 0!
I am an IdentityDictionary that binds its keys to the global symbol namespace.

Needless to say that this is only done for keys that are symbols.!

!WeakSymbolInEnvironmentSet commentStamp: '<historical>' prior: 0!
I am a weak set of symbols bound in an environment.!

!AbstractSymbolInEnvironment methodsFor: 'accessing' stamp: 'KLG 10/9/2020 21:21:59'!
environment
	"Answer the symbol's environment."

	^ self class environment! !

!AbstractSymbolInEnvironment methodsFor: 'converting' stamp: 'KLG 11/11/2020 14:16:52'!
forEnvironment: anEnvironment
	"Answer new symbol for anEnvironment."

	self environment == anEnvironment
		ifFalse: [ ^ AbstractSymbolInEnvironment intern: self asString for: anEnvironment ]! !

!AbstractSymbolInEnvironment methodsFor: 'converting' stamp: 'KLG 11/11/2020 14:17:03'!
forGlobalNamespace
	"Answer global version of mine.
	
	Just answer the global symbol."
	
	^ Symbol intern: self asString! !

!AbstractSymbolInEnvironment methodsFor: 'evaluating' stamp: 'KLG 10/11/2020 22:12:35'!
doWithEnvironmentIfAware: aBlock
	"Evaluate aBlock with my environment, because I am environment aware."
	
	^ aBlock value: self environment! !

!AbstractSymbolInEnvironment class methodsFor: 'accessing' stamp: 'KLG 10/9/2020 20:51:39'!
environment
	"Answer the environment."

	^ environment! !

!AbstractSymbolInEnvironment class methodsFor: 'accessing' stamp: 'KLG 10/9/2020 20:52:10'!
environment: anEnvironment
	"Set the environment."

	self assert: environment isNil.
	environment _ anEnvironment ! !

!AbstractSymbolInEnvironment class methodsFor: 'accessing' stamp: 'KLG 10/9/2020 20:54:10'!
subclassesByEnvironment
	"Answer the mapping from environment to my proper subclass."

	^ SubclassesByEnvironment ifNil: [
		SubclassesByEnvironment _ WeakIdentityKeyDictionary new ]! !

!AbstractSymbolInEnvironment class methodsFor: 'instance creation' stamp: 'KLG 10/9/2020 20:58:14'!
intern: aStringOrSymbol

	^self lookup: aStringOrSymbol :: 		ifNil: 			[
		newSymbols add:
			(aStringOrSymbol class == self ::
				ifTrue: [ aStringOrSymbol ]
				ifFalse: [ (self new: aStringOrSymbol size) string: aStringOrSymbol ]) ]! !

!AbstractSymbolInEnvironment class methodsFor: 'instance creation' stamp: 'KLG 10/9/2020 20:42:28'!
intern: aStringOrSymbol for: anEnvironment

	^self subclassFor: anEnvironment :: intern: aStringOrSymbol! !

!AbstractSymbolInEnvironment class methodsFor: 'instance creation' stamp: 'KLG 10/9/2020 20:40:15'!
lookup: aStringOrSymbol

	^(symbolTable like: aStringOrSymbol) ifNil: [
		newSymbols like: aStringOrSymbol
	]! !

!AbstractSymbolInEnvironment class methodsFor: 'class initialization' stamp: 'KLG 11/11/2020 19:17:34'!
rehash
	"Rebuild the hash table, reclaiming unreferenced Symbols."

	symbolTable _ WeakSymbolInEnvironmentSet withAll: self allInstances.
	newSymbols _ WeakSymbolInEnvironmentSet new.! !

!AbstractSymbolInEnvironment class methodsFor: 'subclass picking' stamp: 'KLG 10/9/2020 21:05:30'!
subclassFor: anEnvironment
	"Answer the appriate subclass for anEnvironment"

	^ self subclassesByEnvironment
		at: anEnvironment
		ifAbsentPut: [ 
			ClassBuilder new
				newSubclassOf: self 
				type: self typeOfClass
				instanceVariables: #()
				from: nil :: rehash environment: anEnvironment ]! !

!AbstractSymbolInEnvironment class methodsFor: 'resetting' stamp: 'KLG 10/25/2020 21:46:41'!
removeAllSubclasses
	"Remove all my subclasses."
	
	"self flagKLG: 'TODO: Remove those that are not used in environments'."
	self flag: #flagKLG:.
	subclasses _ nil! !

!EnvironmentAwareSymbol methodsFor: 'converting' stamp: 'KLG 10/9/2020 20:22:51'!
asEnvironmentAwareSymbol
	"Become an environment aware symbol.
	
	Answer me, as I am aware of environments."
! !

!EnvironmentAwareSymbol methodsFor: 'converting' stamp: 'KLG 10/9/2020 20:33:34'!
forEnvironment: anEnvironment
	"Answer new symbol for anEnvironment."

	^ AbstractSymbolInEnvironment intern: self for: anEnvironment ! !

!EnvironmentBindingIdentityDictionary methodsFor: 'accessing' stamp: 'KLG 10/9/2020 21:56:13'!
at: environmentUnawareKey put: anObject
	"Set the value at key to be anObject. 
	If key is not found, create a new entry for key and set is value to anObject.
	If key is found, update the existing association.
	Answer anObject."

	| index assoc key |
	index _ super findElementOrNil: 
		(key _ environmentUnawareKey forEnvironmentIfAware: environment).
	assoc _ array at: index.
	assoc
		ifNil: [ self atNewIndex: index put: (Association key: key value: anObject) ]
		ifNotNil: [ assoc value: anObject ].
	^ anObject! !

!EnvironmentBindingIdentityDictionary methodsFor: 'accessing' stamp: 'KLG 10/10/2020 20:20:36'!
environment
	"Answer the value of environment"

	^ environment! !

!EnvironmentBindingIdentityDictionary methodsFor: 'accessing' stamp: 'KLG 10/28/2020 18:24:35'!
environment: anObject
	"Set the value of environment"

	self ifNotEmpty: [ ^ self error: 'I must be empty to set my environment' ].
	environment _ anObject! !

!EnvironmentBindingIdentityDictionary methodsFor: 'private' stamp: 'KLG 10/9/2020 21:48:04'!
scanFor: anObject
	"Scan the key array for the first slot containing either a nil (indicating an empty slot) or an element that matches anObject. Answer the index of that slot or zero if no slot is found. This method will be overridden in various subclasses that have different interpretations for matching elements.

Search for an environment aware version of anObject."
	
	^ super scanFor: (anObject forEnvironmentIfAware: environment)! !

!EnvironmentBindingIdentityDictionary methodsFor: 'adding' stamp: 'KLG 10/13/2020 10:39:40'!
add: anAssociation
	"Quite like doing
		self at: anAssociation key put: anAssociation value
	but making sure the argument is stored.
	This method should be used when the argument is stored elsewhere,
	and its value should be kept in sync with the value stored in me.

	If the key already exists, and it is desired to keep the existing association, then call #at:put:
	
	We may copy the association passed in."

	| index key keyInEnvironment associationToAdd |
	keyInEnvironment _ (key _ anAssociation key) forEnvironmentIfAware: environment.
	keyInEnvironment == key
		ifTrue: [ associationToAdd _ anAssociation ]
		ifFalse: [
			associationToAdd _ anAssociation shallowCopy.
			associationToAdd key: keyInEnvironment ].
	index _ super findElementOrNil: keyInEnvironment.
	(array at: index)
		ifNil: [
			self
				atNewIndex: index
				put: associationToAdd ]
		ifNotNil: [
			array
				at: index
				put: associationToAdd ].
	^ anAssociation.! !

!EnvironmentBindingIdentityDictionary class methodsFor: 'instance creation' stamp: 'KLG 10/10/2020 20:22:15'!
forEnvironment: anEnvironment
	"Create the dictionary for an environment."

	^ self new environment: anEnvironment; yourself! !

!GlobalBindingIdentityDictionary methodsFor: 'private' stamp: 'KLG 10/11/2020 15:12:40'!
scanFor: anObject
	"Scan the key array for the first slot containing either a nil (indicating an empty slot) or an element that matches anObject. Answer the index of that slot or zero if no slot is found. This method will be overridden in various subclasses that have different interpretations for matching elements.

Search for an environment aware version of anObject."
	
	^ super scanFor: anObject forGlobalNamespace! !

!GlobalBindingIdentityDictionary methodsFor: 'accessing' stamp: 'KLG 10/11/2020 15:13:30'!
at: environmentAwareKey put: anObject
	"Set the value at key to be anObject. 
	If key is not found, create a new entry for key and set is value to anObject.
	If key is found, update the existing association.
	Answer anObject."

	| index assoc key |
	index _ super findElementOrNil: 
		(key _ environmentAwareKey forGlobalNamespace).
	assoc _ array at: index.
	assoc
		ifNil: [ self atNewIndex: index put: (Association key: key value: anObject) ]
		ifNotNil: [ assoc value: anObject ].
	^ anObject! !

!GlobalBindingIdentityDictionary methodsFor: 'adding' stamp: 'KLG 10/11/2020 15:15:08'!
add: anAssociation
	"Quite like doing
		self at: anAssociation key put: anAssociation value
	but making sure the argument is stored.
	This method should be used when the argument is stored elsewhere,
	and its value should be kept in sync with the value stored in me.

	If the key already exists, and it is desired to keep the existing association, then call #at:put:
	
	We' l modify the association passed in"

	| index key |
	anAssociation key: (key _ anAssociation key forGlobalNamespace).
	index _ super findElementOrNil: key.
	(array at: index)
		ifNotNil: [ array at: index put: anAssociation ]
		ifNil: [ self atNewIndex: index put: anAssociation ].
	^ anAssociation! !

!GlobalBindingIdentityDictionary class methodsFor: 'instance creation' stamp: 'KLG 10/11/2020 15:09:18'!
forEnvironment: anEnvironment
	"Create the dictionary for an environment."

	^ self new environment: anEnvironment; yourself! !

!WeakSymbolInEnvironmentSet methodsFor: 'private' stamp: 'KLG 11/11/2020 19:16:31'!
scanFor: anObject
	"Scan the key array for the first slot containing either a nil (indicating an empty slot) or an element that matches anObject. Answer the index of that slot or zero if no slot is found. This method will be overridden in various subclasses that have different interpretations for matching elements"

	| element start finish |

	start _ (anObject hash \\ array size) + 1.
	finish _ array size.

	"Search from (hash mod size) to the end."
	start to: finish do:
		[:index | ((element _ array at: index) == flag or: [
				self compareElementWise: element with: anObject ] )
			ifTrue: [^ index ]].

	"Search from 1 to where we started."
	1 to: start-1 do:
		[:index | ((element _ array at: index) == flag or: [
				self compareElementWise: element with: anObject ])
			ifTrue: [^ index ]].

	^ 0  "No match AND no empty slot"! !

!WeakSymbolInEnvironmentSet methodsFor: 'comparing' stamp: 'Install-ModulesTools 11/11/2020 19:28:34'!
compareElementWise: symbol1 with: symbol2
	"comment stating purpose of message"

	| symbol1Size symbol2Size |
	symbol1Size _ [ symbol1 size ] on: Error do: [ ^ false ].
	symbol2Size _ [ symbol2 size ] on: Error do: [ ^ false ].
	symbol1Size = symbol2Size ifFalse: [ ^ false ].
	symbol1 with: symbol2 do: [ :char1 :char2 |
		char1 = char2 ifFalse: [ ^ false ] ].
	^ true! !

!ProtoObject methodsFor: '*EnvironmentAwareness' stamp: 'KLG 10/11/2020 22:05:58'!
doWithEnvironmentIfAware: aBlock
	"Evaluate aBlock with my environment, if I am environment aware.
	
	Do nothing here, because I don't know nothing about no environments."! !

!ProtoObject methodsFor: '*EnvironmentAwareness' stamp: 'KLG 10/9/2020 21:36:59'!
forEnvironmentIfAware: anEnvironment
	"Answer an environment version of mine.

	I am not environment aware, so answer me."! !

!ProtoObject methodsFor: '*EnvironmentAwareness' stamp: 'KLG 10/11/2020 15:11:08'!
forGlobalNamespace
	"Answer global version of mine.

	I am not environment aware, that is I am always global, so answer me."! !

!Symbol methodsFor: '*EnvironmentAwareness' stamp: 'KLG 11/11/2020 11:27:56'!
= another

	"Use == between two symbols..."
	self == another ifTrue: [
		^ true].  "Was == "
	another isKindOf: Symbol :: ifTrue: [
		^ false].  "Was not == "

	"Otherwise use string =..."
	^ super = another! !

!Symbol methodsFor: '*EnvironmentAwareness' stamp: 'KLG 10/9/2020 21:04:39'!
asEnvironmentAwareSymbol
	"Become an environment aware symbol."

	{ self } elementsForwardIdentityTo: { EnvironmentAwareSymbol new: self size :: string: self }! !

!Symbol methodsFor: '*EnvironmentAwareness' stamp: 'KLG 10/9/2020 21:02:42'!
forEnvironment: anEnvironment
	"Answer new symbol for anEnvironment."

	^ self asEnvironmentAwareSymbol forEnvironment: anEnvironment! !

!Symbol methodsFor: '*EnvironmentAwareness' stamp: 'KLG 10/10/2020 21:11:07'!
forEnvironmentIfAware: anEnvironment
	"Answer an environment version of mine.
	
	I am environment aware, so convert me if necessary."
	
	^ self forEnvironment: anEnvironment! !
