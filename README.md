**IMPORTANT NOTE: This repo will soon be readonly. See [Haver](https://hg.sr.ht/~cy-de-fect/HaverOnCuis) hosted on [sourcehut](https://sr.ht/).**

# Environments

My take on environments aka. namespaces for Cuis.


## Basic Usage

For the time being there a simple implementation of environments,
called "simple environments". It resides in a package `SimpleEnvironments`.

Simple environments have no notion of a current environment.
Classes in an environment are defined by a special class definition
statement like this:
```smalltalk
    Object subclass: #TowersOfHanoi
	    instanceVariableNames: 'numberOfDisks currentStep steps sourceTower targetTower'
	    classVariableNames: ''
	    poolDictionaries: ''
	    category: 'EnvironmentsTrial'
	    inSimpleEnvironment: #Trials
```
If you want to reference `TowersOfHanoi` you have to use the appropriate message:
```smalltalk
SimpleEnvironments>>#Trials>>#TowersOfHanoi
```
So if you want to subclass `TowersOfHanoi` you can do like this:
```smalltalk
(SimpleEnvironments>>#Trials>>#TowersOfHanoi) subclass: #GlobalTowersOfHanoi
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentsTrial'
```
As the examples wording suggests, this will create a subclass of `TowesOfHanoi`
bound in the global namepspace (`Smalltalk`). You can use the class list menu's
'subclass template' menu item to generate a template for a new class.

This will create a subclass of `TowersOfHanoi` in simple environment called
`#MyEnvironment`:
```smalltalk
(SimpleEnvironments>>#Trials>>#TowersOfHanoi) subclass: #TowersOfHanoi
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentsTrial'
	inSimpleEnvironment: #MyEnvironment
```

## Installation

Environments was tested with a clean image of versions 4428 and 4434.

 - Prepare a clean image of one of the version mentioned above.
 - Clone the [environments repo](https://gitlab.com/klgcuisstuff/environments) 
   from [GitLab](https://gitlab.com).
 - In the repo's directory a sub-directory named `coreChanges` exists.
   In the directory you will find a change-set in a file named
   `4393-CuisCore-GeraldKlix-2020Oct07-15h40m-KLG.cs.st`.
   Install the change-set in that file.
 - Install the `SimpleEnvironments` package in the corresponding
   file.

**Note: It is necessary to close all browser windows when installing those packages!**

## Documentation

A detailed documentation of the Environments implementation can be found
in [environments documentation package](EnvironmentsDocumentation.pck.st).
