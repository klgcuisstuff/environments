'From Cuis 5.0 [latest update: #4396] on 15 October 2020 at 3:58:27 pm'!
'Description '!
!provides: 'EnvironmentsTrial' 1 2!
SystemOrganization addCategory: 'EnvironmentsTrial'!


!classDefinition: (SimpleEnvironments>>#Trials>>#TowersOfHanoi) category: 'EnvironmentsTrial'!
Object subclass: #TowersOfHanoi
	instanceVariableNames: 'numberOfDisks currentStep steps sourceTower targetTower'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentsTrial'
	inSimpleEnvironment: #Trials!
!classDefinition: (SimpleEnvironments>>#Trials>>#TowersOfHanoi) class category: 'EnvironmentsTrial'!
(SimpleEnvironments>>#Trials>>#TowersOfHanoi) class
	instanceVariableNames: ''!


!(SimpleEnvironments>>#Trials>>#TowersOfHanoi) commentStamp: '<historical>' prior: 0!
I am to compute the towers of hanoi sequences. !

!(SimpleEnvironments>>#Trials>>#TowersOfHanoi) methodsFor: 'initialization' stamp: 'KLG 10/15/2020 15:44:30'!
initialize
	"Initialize me."

	super initialize.
	steps _ OrderedCollection new.
	currentStep _ 1.! !

!(SimpleEnvironments>>#Trials>>#TowersOfHanoi) methodsFor: 'accessing' stamp: 'KLG 10/15/2020 15:54:31'!
steps
	"Answer the steps"

	^ steps! !

!(SimpleEnvironments>>#Trials>>#TowersOfHanoi) methodsFor: 'play' stamp: 'KLG 10/15/2020 15:53:08'!
computeMoveFrom: aSourceTower to: aTargetTower disks: aNumberOfDisks.
	"comment stating purpose of message"

	aNumberOfDisks = 0
		ifFalse: [ | intermediateTower |
			intermediateTower _ 6 - aSourceTower - aTargetTower.
			self computeMoveFrom: aSourceTower to: intermediateTower disks: aNumberOfDisks - 1.
			steps add: aSourceTower -> aTargetTower.
			self computeMoveFrom: intermediateTower to: aTargetTower disks: aNumberOfDisks - 1 ]
! !

!(SimpleEnvironments>>#Trials>>#TowersOfHanoi) methodsFor: 'play' stamp: 'KLG 10/15/2020 15:54:43'!
startWith: aNumberOfDisks
	"Start with a numbe rof disks."

	^ self startWith: aNumberOfDisks at: 1 to: 2! !

!(SimpleEnvironments>>#Trials>>#TowersOfHanoi) methodsFor: 'play' stamp: 'KLG 10/15/2020 15:54:39'!
startWith: aNumberOfDisks at: aSourceTower to: aTargetTower
	"Start with a numbe rof disks."

	self assert: numberOfDisks isNil.
	numberOfDisks _ aNumberOfDisks.
	currentStep _ 1.
	sourceTower _ aSourceTower.
	targetTower _ aTargetTower.
	self computeMoveFrom: sourceTower to: targetTower disks: numberOfDisks.
	^ steps! !
