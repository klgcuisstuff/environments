
Smalltalk at: #CodeFileBrowser ifPresent: [ :class | class definition = 'Browser subclass: #CodeFileBrowser
	instanceVariableNames: ''baseCodeSource caseCodeSource''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Tools-Code File Browser''' ifFalse: [ self error: 'Class definition mismatch: CodeFileBrowser'] ].
Smalltalk at: #Metaclass ifPresent: [ :class | class definition = 'ClassDescription subclass: #Metaclass
	instanceVariableNames: ''thisClass''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Kernel-Classes''' ifFalse: [ self error: 'Class definition mismatch: Metaclass'] ].
Smalltalk at: #CodeFile ifPresent: [ :class | class definition = 'Object subclass: #CodeFile
	instanceVariableNames: ''fullName sourceSystem classes doIts classOrder''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Tools-Code File Browser''' ifFalse: [ self error: 'Class definition mismatch: CodeFile'] ].
Smalltalk at: #Class ifPresent: [ :class | class definition = 'ClassDescription subclass: #Class
	instanceVariableNames: ''subclasses name classPool sharedPools''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Kernel-Classes''' ifFalse: [ self error: 'Class definition mismatch: Class'] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class definition = 'IdentityDictionary subclass: #SystemDictionary
	instanceVariableNames: ''cachedClassNames cachedNonClassNames startUpScriptArguments startUpArguments''
	classVariableNames: ''ChangesInitialFileSize EndianCache LastImageName LastQuitLogPosition LowSpaceProcess LowSpaceSemaphore ShutDownList SourceFileVersionString SpecialSelectors StartUpList StartupStamp WordSize''
	poolDictionaries: ''''
	category: ''System-Support''' ifFalse: [ self error: 'Class definition mismatch: SystemDictionary'] ].
Smalltalk at: #ChangeList ifPresent: [ :class | class definition = 'CodeProvider subclass: #ChangeList
	instanceVariableNames: ''changeList list listIndex listSelections file''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Tools-Changes''' ifFalse: [ self error: 'Class definition mismatch: ChangeList'] ].
Smalltalk at: #ClassBuilder ifPresent: [ :class | class definition = 'Object subclass: #ClassBuilder
	instanceVariableNames: ''instVarMap progress maxClassIndex currentClassIndex''
	classVariableNames: ''QuietMode''
	poolDictionaries: ''''
	category: ''Kernel-Classes''' ifFalse: [ self error: 'Class definition mismatch: ClassBuilder'] ].
Smalltalk at: #BasicClassOrganizer ifPresent: [ :class | class definition = 'Categorizer subclass: #BasicClassOrganizer
	instanceVariableNames: ''subject classComment commentStamp''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Kernel-Classes''' ifFalse: [ self error: 'Class definition mismatch: BasicClassOrganizer'] ].
Smalltalk at: #HierarchyBrowser ifPresent: [ :class | class definition = 'Browser subclass: #HierarchyBrowser
	instanceVariableNames: ''classList centralClass''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Tools-Browser''' ifFalse: [ self error: 'Class definition mismatch: HierarchyBrowser'] ].
Smalltalk at: #ClassDescription ifPresent: [ :class | class definition = 'Behavior subclass: #ClassDescription
	instanceVariableNames: ''instanceVariables organization''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Kernel-Classes''' ifFalse: [ self error: 'Class definition mismatch: ClassDescription'] ].
Smalltalk at: #Object ifPresent: [ :class | class definition = 'ProtoObject subclass: #Object
	instanceVariableNames: ''''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Kernel-Objects''' ifFalse: [ self error: 'Class definition mismatch: Object'] ].
Smalltalk at: #Browser ifPresent: [ :class | class definition = 'CodeProvider subclass: #Browser
	instanceVariableNames: ''systemOrganizer classOrganizer metaClassOrganizer selectedSystemCategory selectedClassName selectedMessageCategory selectedMessage editSelection metaClassIndicated listClassesHierarchically''
	classVariableNames: ''RecentClasses''
	poolDictionaries: ''''
	category: ''Tools-Browser''' ifFalse: [ self error: 'Class definition mismatch: Browser'] ].
Smalltalk at: #ProtocolBrowser ifPresent: [ :class | class definition = 'MessageSet subclass: #ProtocolBrowser
	instanceVariableNames: ''baseClass selectiveClassList selectiveClassListIndex selectedName exclude''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Tools-Browser''' ifFalse: [ self error: 'Class definition mismatch: ProtocolBrowser'] ].
Smalltalk at: #CodeFileBrowserWindow ifPresent: [ :class | class definition = 'BrowserWindow subclass: #CodeFileBrowserWindow
	instanceVariableNames: ''''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''Morphic-Tool Windows''' ifFalse: [ self error: 'Class definition mismatch: CodeFileBrowserWindow'] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #removeSelector:  ifPresent: [ :method | method timeStamp = 'sma 6/18/2000 11:34' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#removeSelector:'] ] ].
Smalltalk at: #Browser ifPresent: [ :class | class methodDict at: #classListIndex:  ifPresent: [ :method | method timeStamp = 'SLD 2/8/2019 22:07:43' ifFalse: [ self error: 'Timestamp mismatch: Browser>>#classListIndex:'] ] ].
Smalltalk at: #Browser ifPresent: [ :class | class methodDict at: #moveAllToOtherSystemCategory  ifPresent: [ :method | method timeStamp = 'pb 5/7/2019 21:21:40' ifFalse: [ self error: 'Timestamp mismatch: Browser>>#moveAllToOtherSystemCategory'] ] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #at:put:  ifPresent: [ :method | method timeStamp = 'jmv 1/3/2016 00:22' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#at:put:'] ] ].
Smalltalk at: #ChangeList ifPresent: [ :class | class methodDict at: #scanClassComment:  ifPresent: [ :method | method timeStamp = 'HAW 10/26/2019 11:30:10' ifFalse: [ self error: 'Timestamp mismatch: ChangeList>>#scanClassComment:'] ] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #prepareToRenameClass:as:  ifPresent: [ :method | method timeStamp = 'HAW 12/4/2019 10:54:31' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#prepareToRenameClass:as:'] ] ].
Smalltalk at: #HierarchyBrowser ifPresent: [ :class | class methodDict at: #initHierarchyForClass:  ifPresent: [ :method | method timeStamp = 'sw 5/8/2000 01:02' ifFalse: [ self error: 'Timestamp mismatch: HierarchyBrowser>>#initHierarchyForClass:'] ] ].
Smalltalk at: #ProtocolBrowser ifPresent: [ :class | class methodDict at: #hierarchyForClass:  ifPresent: [ :method | method timeStamp = 'dhn 10/31/2017 17:38:39' ifFalse: [ self error: 'Timestamp mismatch: ProtocolBrowser>>#hierarchyForClass:'] ] ].
Smalltalk at: #ChangeList ifPresent: [ :class | class methodDict at: #classDefinitionRecordFrom:  ifPresent: [ :method | method timeStamp = 'HAW 10/29/2019 11:06:27' ifFalse: [ self error: 'Timestamp mismatch: ChangeList>>#classDefinitionRecordFrom:'] ] ].
Smalltalk at: #HierarchyBrowser ifPresent: [ :class | class methodDict at: #classList  ifPresent: [ :method | method timeStamp = 'HAW 8/2/2018 20:06:27' ifFalse: [ self error: 'Timestamp mismatch: HierarchyBrowser>>#classList'] ] ].
Smalltalk at: #ClassDescription ifPresent: [ :class | class methodDict at: #printMethodChunk:withPreamble:on:moveSource:toFile:  ifPresent: [ :method | method timeStamp = 'jmv 3/6/2019 13:07:59' ifFalse: [ self error: 'Timestamp mismatch: ClassDescription>>#printMethodChunk:withPreamble:on:moveSource:toFile:'] ] ].
Smalltalk at: #CodeFileBrowserWindow ifPresent: [ :class | class methodDict at: #findClass  ifPresent: [ :method | method timeStamp = 'len 5/20/2020 06:54:25' ifFalse: [ self error: 'Timestamp mismatch: CodeFileBrowserWindow>>#findClass'] ] ].
Smalltalk at: #Browser ifPresent: [ :class | class methodDict at: #classListIndexOf:  ifPresent: [ :method | method timeStamp = 'jmv 8/4/2018 14:48:21' ifFalse: [ self error: 'Timestamp mismatch: Browser>>#classListIndexOf:'] ] ].
Smalltalk at: #ClassBuilder ifPresent: [ :class | class methodDict at: #privateNewSubclassOf:from:  ifPresent: [ :method | method timeStamp = 'jmv 5/14/2015 09:16' ifFalse: [ self error: 'Timestamp mismatch: ClassBuilder>>#privateNewSubclassOf:from:'] ] ].
Smalltalk at: #ChangeList ifPresent: [ :class | class methodDict at: #scanMethodDefinition:  ifPresent: [ :method | method timeStamp = 'jmv 4/2/2020 12:51:10' ifFalse: [ self error: 'Timestamp mismatch: ChangeList>>#scanMethodDefinition:'] ] ].
Smalltalk at: #CodeFile ifPresent: [ :class | class methodDict at: #classDefinition:with:  ifPresent: [ :method | method timeStamp = 'jmv 5/20/2015 12:51' ifFalse: [ self error: 'Timestamp mismatch: CodeFile>>#classDefinition:with:'] ] ].
Smalltalk at: #HierarchyBrowser ifPresent: [ :class | class methodDict at: #potentialClassNames  ifPresent: [ :method | method timeStamp = 'HAW 8/2/2018 20:07:42' ifFalse: [ self error: 'Timestamp mismatch: HierarchyBrowser>>#potentialClassNames'] ] ].
Smalltalk at: #BasicClassOrganizer ifPresent: [ :class | class methodDict at: #putCommentOnFile:numbered:moveSource:forClass:  ifPresent: [ :method | method timeStamp = 'ss 10/3/2019 19:58:31' ifFalse: [ self error: 'Timestamp mismatch: BasicClassOrganizer>>#putCommentOnFile:numbered:moveSource:forClass:'] ] ].
Smalltalk at: #CodeFile ifPresent: [ :class | class methodDict at: #getClass:  ifPresent: [ :method | method timeStamp = 'jmv 11/2/2011 15:27' ifFalse: [ self error: 'Timestamp mismatch: CodeFile>>#getClass:'] ] ].
Smalltalk at: #HierarchyBrowser ifPresent: [ :class | class methodDict at: #classListIndex:  ifPresent: [ :method | method timeStamp = 'HAW 8/2/2018 20:06:54' ifFalse: [ self error: 'Timestamp mismatch: HierarchyBrowser>>#classListIndex:'] ] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #fillCaches  ifPresent: [ :method | method timeStamp = 'jmv 9/27/2016 21:36:09' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#fillCaches'] ] ].
Smalltalk at: #ClassDescription ifPresent: [ :class | class methodDict at: #definition  ifPresent: [ :method | method timeStamp = 'HAW 4/12/2018 13:36:40' ifFalse: [ self error: 'Timestamp mismatch: ClassDescription>>#definition'] ] ].
Smalltalk at: #ClassBuilder ifPresent: [ :class | class methodDict at: #privateNewSubclassOf:  ifPresent: [ :method | method timeStamp = 'ar 2/27/2003 22:56' ifFalse: [ self error: 'Timestamp mismatch: ClassBuilder>>#privateNewSubclassOf:'] ] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #allBehaviorsDo:  ifPresent: [ :method | method timeStamp = 'jmv 8/31/2011 22:34' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#allBehaviorsDo:'] ] ].
Smalltalk at: #Browser ifPresent: [ :class | class methodDict at: #categorizeUnderCategoryAt:class:  ifPresent: [ :method | method timeStamp = 'HAW 12/27/2018 13:48:49' ifFalse: [ self error: 'Timestamp mismatch: Browser>>#categorizeUnderCategoryAt:class:'] ] ].
Smalltalk at: #ProtocolBrowser ifPresent: [ :class | class methodDict at: #protocolFor:  ifPresent: [ :method | method timeStamp = 'dhn 10/31/2017 16:56:42' ifFalse: [ self error: 'Timestamp mismatch: ProtocolBrowser>>#protocolFor:'] ] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #hasClassNamed:  ifPresent: [ :method | method timeStamp = 'jmv 9/27/2016 21:29:08' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#hasClassNamed:'] ] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #removeClassNamed:  ifPresent: [ :method | method timeStamp = 'jmv 3/22/2012 22:32' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#removeClassNamed:'] ] ].
Smalltalk at: #Browser ifPresent: [ :class | class methodDict at: #removeSystemCategory  ifPresent: [ :method | method timeStamp = 'jmv 6/8/2011 08:13' ifFalse: [ self error: 'Timestamp mismatch: Browser>>#removeSystemCategory'] ] ].
Smalltalk at: #Class ifPresent: [ :class | class methodDict at: #bindingOf:  ifPresent: [ :method | method timeStamp = 'jmv 5/24/2014 11:00' ifFalse: [ self error: 'Timestamp mismatch: Class>>#bindingOf:'] ] ].
Smalltalk at: #Class class ifPresent: [ :class | class methodDict at: #templateForSubclassOf:category:  ifPresent: [ :method | method timeStamp = 'sw 4/27/2000 16:20' ifFalse: [ self error: 'Timestamp mismatch: Class>>#templateForSubclassOf:category:'] ] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #removeKey:ifAbsent:  ifPresent: [ :method | method timeStamp = 'jmv 7/3/2010 16:38' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#removeKey:ifAbsent:'] ] ].
Smalltalk at: #ProtocolBrowser ifPresent: [ :class | class methodDict at: #selectedClass  ifPresent: [ :method | method timeStamp = 'KenD 9/18/2020 13:21:11' ifFalse: [ self error: 'Timestamp mismatch: ProtocolBrowser>>#selectedClass'] ] ].
Smalltalk at: #ClassDescription ifPresent: [ :class | class methodDict at: #definitionPreambleWithoutStamp  ifPresent: [ :method | method timeStamp = 'HAW 11/9/2019 10:35:24' ifFalse: [ self error: 'Timestamp mismatch: ClassDescription>>#definitionPreambleWithoutStamp'] ] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #classNamed:  ifPresent: [ :method | method timeStamp = 'jmv 5/8/2015 16:12' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#classNamed:'] ] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #renameClassNamed:as:  ifPresent: [ :method | method timeStamp = 'jmv 3/13/2012 12:50' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#renameClassNamed:as:'] ] ].
Smalltalk at: #Class ifPresent: [ :class | class methodDict at: #binding  ifPresent: [ :method | method timeStamp = 'jmv 12/31/2009 09:49' ifFalse: [ self error: 'Timestamp mismatch: Class>>#binding'] ] ].
Smalltalk at: #SystemDictionary ifPresent: [ :class | class methodDict at: #obsoleteClasses  ifPresent: [ :method | method timeStamp = 'jmv 3/1/2010 14:08' ifFalse: [ self error: 'Timestamp mismatch: SystemDictionary>>#obsoleteClasses'] ] ].
Smalltalk at: #Browser ifPresent: [ :class | class methodDict at: #categorizeUnderNewCategoryClass:  ifPresent: [ :method | method timeStamp = 'HAW 12/28/2018 12:38:10' ifFalse: [ self error: 'Timestamp mismatch: Browser>>#categorizeUnderNewCategoryClass:'] ] ].
self inform: 'Baseline check OK'.