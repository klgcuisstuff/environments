'From Cuis 5.0 [latest update: #4434] on 28 October 2020 at 4:06:36 pm'!

!ClassDocLinkRenderer class methodsFor: 'as yet unclassified' stamp: 'KLG 10/28/2020 16:04:50'!
renderLink: aDocLink on: aStream

	aStream nextPut: (Text string: aDocLink labelOrTarget 
				attribute: (BlockTextAction do: [:x | | class className |
						className _ aDocLink target withBlanksTrimmed.
						self environmentImplementationsIfPresentDo: [ :implementations | | tokens |
							tokens _ Scanner new scanTokens: className.
							implementations scanClassDenotation: tokens :: ifNotNil: [ :localNameSymbol |
								className _ localNameSymbol ] ].
						className _ className asSymbol.
						class _ Smalltalk 
									at: className
									ifAbsent: [self error: 'Class not found: ', className asString].
						self browse: class selector: nil]))! !

