'From Cuis 5.0 [latest update: #4434] on 29 October 2020 at 6:24:53 pm'!
'Description Erudite documentation of Environments in Cuis.'!
!provides: 'EnvironmentsDocumentation' 1 15!
!requires: 'Erudite' 1 144 nil!
!requires: 'SimpleEnvironments' 1 22 nil!
SystemOrganization addCategory: 'EnvironmentsDocumentation'!


!classDefinition: #EnvironmentsDocumentation category: 'EnvironmentsDocumentation'!
EruditeBook subclass: #EnvironmentsDocumentation
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EnvironmentsDocumentation'!
!classDefinition: 'EnvironmentsDocumentation class' category: 'EnvironmentsDocumentation'!
EnvironmentsDocumentation class
	instanceVariableNames: ''!


!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
Anatomy
^(EruditeBookSection basicNew title: 'Anatomy'; document: ((EruditeDocument contents: '!!!! The Environment Implemantation''s Anatomy

The Environments implementation consists of a big change set, that
prepares Cuis'' core image for environments and two packages.

 - An [[[
browser _ SinglePackageBrowser new.
browser package: (CodePackage named: ''EnvironmentAwareness'' createIfAbsent: false registerIfNew: false).
BrowserWindow open: browser label: browser labelString
]]] doItWithButton: EnvironmentAwareness. package, that provides environment aware symbols.
 - A [[[
browser _ SinglePackageBrowser new.
browser package: (CodePackage named: ''EnvironmentsBase'' createIfAbsent: false registerIfNew: false).
BrowserWindow open: browser label: browser labelString
]]] doItWithButton: EnvironmentsBase. package, that provides the basic infrastructure for environments.

There is also a [[[
browser _ SinglePackageBrowser new.
browser package: (CodePackage named: ''SimpleEnvironments'' createIfAbsent: false registerIfNew: false).
BrowserWindow open: browser label: browser labelString
]]] doItWithButton: simple demo environments implementation., that
serves as an example for an environment implementation.') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new        add: self AnatomyChangeSet;
        add: self AnatomyEnvironmentAwareness;
        add: self AnatomyEnvironmentsBase;
 yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 11:23:19'!
AnatomyBaseEnvironments
^(EruditeBookSection basicNew title: 'BaseEnvironments'; document: ((EruditeDocument contents: '!!!! Environment Base Classes

') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyChangeSet
^(EruditeBookSection basicNew title: 'ChangeSet'; document: ((EruditeDocument contents: '!!!! The Monster Change Set

The implementation consists of rather huge change set, that patches
various classes and methods to deal with environments.

Alas, implementing environments caused a lot of code in Cuis'' core
to break, although this was not wanted by the author.

The change set is logically divided into groups.') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new        add: self AnatomyChangeSetConditionalExecution;
        add: self AnatomyChangeSetClassDefinition;
        add: self AnatomyChangeSetCompilation;
        add: self AnatomyChangeSetSystemDictionary;
        add: self AnatomyChangeSetSourceCodeManagement;
        add: self AnatomyChangeSetEnvironmentAwareSymbols;
 yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyChangeSetClassDefinition
^(EruditeBookSection basicNew title: 'ClassDefinition'; document: ((EruditeDocument contents: '!!!! Changes Related to Class Defintions and Class Denotations

!!!!!! Environment-aware Subclass Creation

To allow extended class creation messages {ClassBuilder ::class} was refactored.
Direct references to {Metclass ::class} were factored out into {metaclassClass ::selector}.
{ClassBuilder>>privateNewSubclassOf: ::method} was changed accordingly.
{ClassBuilder>>privateNewSubclassOf:from: ::method} used {shallowCopy ::selector}
to create a new meta class. The sending of {shallowCopy ::selector} was factored out into
{ClassBuilder>>newMetaclassFromOldMetaclass: ::method} to allow subclasses of {ClassBuilder ::class}
to create copies of a meta class in a different way.
Furthermore {ClassBuilder>>configureNewMetaclass: ::method} is sent to [[[self]]]
whenever a new metaclass was created. This allows subclasses of {ClassBuilder ::class} to
configure the newly created metaclass. This is used in 
{AbstractEnvironmentClassBuilder>>configureNewMetaclass: ::method} to implement
a double dispatch:

{AbstractEnvironmentClassBuilder>>configureNewMetaclass: ::method}[embed]

The double dispatch avoids calling setters, instead the metaclass can retrieve
information from the class builder, as shown here:

{SimpleEnvironmentMetaclass>>configureFromClassBuilder: ::method}[embed]

All this changes allow us to implement gems like [[[Class>>#subclass:instanceVariableNames:classVariableNames:poolDictionaries:category:inSimpleEnvironment: :: browse]]] doItWithButton: methods, that create subclasses in environments.:
{Class>>subclass:instanceVariableNames:classVariableNames:poolDictionaries:category:inSimpleEnvironment: ::method}[embed]


!!!!!! Class Defintions and Denotations

In order to allow a class to print itself {ClassDescription>>definition ::method}
was refactored:

{ClassDescription>>definition ::method}[embed]

{name ::selector} was replaced with {safeDenotingExpression ::selector}.
{printAdditionalDefinitionOn: ::selector} is sent to [[[self]]] after
the normal part of the class description was written on the stream.
{Class>>printAdditionalDefinitionOn: ::method} deferrs printing
to its metaclass.

This, for example, allows {SimpleEnvironmentMetaclass ::class} to add
the ```inSimpleEnvironment:```-phrase to the end of the class description:

{SimpleEnvironmentMetaclass>>printAdditionalDefinitionOn: ::method}[embed]

{denotingExpression ::selector} is deferred to the metaclass in {Class>>denotingExpression ::method}:

{Class>>denotingExpression ::method}[embed]

{Metaclass>>denotingExpressionForClass ::method} just answers the class''s name.

{SimpleEnvironmentMetaclass>>denotingExpressionForClass ::method} does some
footwork to make class defintions like this possible:

[[[Object subclass: #DocuClass1
	instanceVariableNames: ''''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''SimpleEnvironmentsTest - TestClasses''
	inSimpleEnvironment: #DocuEnv]]] printItHere

**Note how the class prints an expression, that evaluates to itself!!**

Of course we can do things like this:

[[[SimpleEnvironments>>#DocuEnv>>#DocuClass1 subclass: #DocuClass2
	instanceVariableNames: ''''
	classVariableNames: ''''
	poolDictionaries: ''''
	category: ''SimpleEnvironmentsTest - TestClasses''
	inSimpleEnvironment: #DocuEnv]]] printItHere

[[[SimpleEnvironments>>#DocuEnv>>#DocuClass2]]] printItHere

obviously evaluates to the right class.

If you haven''t installed the changeset in file ```ChangesTo-Erudite-KLG.cs.st```
```{SimpleEnvironments>>#DocuEnv>>#DocuClass2 ::class}``` does not work in Erudite,
because Erudite is not aware of the extended syntax:
{SimpleEnvironments>>#DocuEnv>>#DocuClass2 ::class}

The {safeDenotingExpression ::selector} provides some means to generate an expression
that can be safely used in every syntactic context; it is the answer of {denotingExpression ::selector}
in parentheses, if necessary. This method allows expression like this one

[[[SimpleEnvironments>>#DocuEnv>>#DocuClass2 :: class]]] printItHere

a reasonable answer.') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyChangeSetCompilation
^(EruditeBookSection basicNew title: 'Compilation'; document: ((EruditeDocument contents: '!!!! Compilation Related Changes

Compilation related changes almost exclusivly deal with {bindingOf: ::selector}-like
methods in {Class ::class} and {Metaclass ::class}. {Class>>bindingOf: ::method} was
changed. It now delegates non-local lookups by sending {nonLocalBindingOf: ::selector}
to [[[self]]]:

{Class>>bindingOf: ::method} [embed]

{Class>>nonLocalBindingOf: ::method} deferrs lookup to its metaclass
by sending it {forNonLocalBindingEnvironmentsDo: ::selector}:

{Class>>nonLocalBindingOf: ::method}[embed]

It stops the iteration when the first environment answers a non-[[[nil]]]
binding.

{Metaclass>>forNonLocalBindingEnvironmentsDo: ::method} just 
"iterates" over the global environment (```Smalltalk```):

{Metaclass>>forNonLocalBindingEnvironmentsDo: ::method}[embed]

{forNonLocalBindingEnvironmentsDo: ::selector} is intended to be overriden
in subclasses. {SimpleEnvironmentMetaclass>>forNonLocalBindingEnvironmentsDo: ::method}
is an example that just tries the environment''s {SimpleEnvironment>>bindingOf: ::method}
before resorting to it''s superclass:

{SimpleEnvironmentMetaclass>>forNonLocalBindingEnvironmentsDo: ::method}[embed]

') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyChangeSetConditionalExecution
^(EruditeBookSection basicNew title: 'ConditionalExecution'; document: ((EruditeDocument contents: '!!!! Conditional Execution of Environment Specific Code

Code that only needs to be executed is encapsulated in blocks that are only
evaluated, when {Object>>environmentImplementationsIfPresentDo: ::method}
detects the presence of an actual environment implementation.

In fact this method is implemented as a no-op in the change set, but
replaced with an extension method that does evaluate the block.

The aforementioned message is mostly sent in the source code
management classes like {CodeFile ::class}, {CodeFileBrowser ::class} and
{ChangeList ::class}. Of course it is often used in {System	Dictionary ::class}.') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyChangeSetEnvironmentAwareSymbols
^(EruditeBookSection basicNew title: 'EnvironmentAwareSymbols'; document: ((EruditeDocument contents: '!!!! Changes Caused by The Use of Environment Aware Symbols

The use of environment aware symbols caused some issues with tools that
do not treat symbols (Instances of {Symbol ::class}) as atomic entities.


!!!!!! Browser

{Browser ::class} has a method to compute the list of classes to show.
Alas this list may contain indented symbols (dependeing on a preference).
I added a method that does the computation, but leaves the class name symbols
intact:

{Browser>>hierarchicalClassNamesList ::method}[embed]

This message is sent by {Browser>>classNamesListForSelecting ::method}
which in turn is used by [[[Smalltalk browseAllCallsOn: #classNamesListForSelecting]]] doItWithButton:
various method concerned with selection of the class to browse..


!!!!!! HierarchyBrowser

{HierarchyBrowser ::class} had the same problem, here the indented list is stored in an instance variable.
I added another instance variable that contains the unaltered symbol:

The modified {HierarchyBrowser>>initHierarchyForClass: ::method} fills both instance variables:

{HierarchyBrowser>>initHierarchyForClass: ::method}[embed]

{HierarchyBrowser ::class} also redefines {classNamesListForSelecting ::selector}:

{HierarchyBrowser>>classNamesListForSelecting ::method}[embed]
') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/28/2020 19:31:11'!
AnatomyChangeSetEnvironmentAwareness
^(EruditeBookSection basicNew title: 'EnvironmentAwareness'; document: ((EruditeDocument contents: '!!!! Changes Caused by The Use of Environment Aware Symbols') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyChangeSetSourceCodeManagement
^(EruditeBookSection basicNew title: 'SourceCodeManagement'; document: ((EruditeDocument contents: '!!!! Changes Related to Source Code Management

In order to deal with extended class defintions some changes to {ClassList ::class}
where necessary. A method to scan these extended class defintions was added:

!!!!!! ChangeList

{ChangeList>>scanClassDenotation: ::method} [embed]```

Some scanning methods such as:

{ChangeList>>scanMethodDefinition: ::method}[embed]

send this message to [[[self]]].
The other methods are {ChangeList>>classDefinitionRecordFrom: ::method}and
{ChangeList>>scanClassComment: ::method}.


!!!!!! CodeFile

{CodeFile ::class} has a similar method:

{CodeFile>>scanClassDenotation: ::method}[embed]

The message {scanClassDenotation: ::selector} is sent from:

{CodeFile>>getClass: ::method}[embed],

{CodeFile>>classDefinition:with: ::method} was changed to deal with the varying number
of tokens in class definitions:

{CodeFile>>classDefinition:with: ::method}[embed]


!!!!!! BasicClassOrganizer

{BasicClassOrganizer>>putCommentOnFile:numbered:moveSource:forClass: ::method} was changed to send
{safeDenotingExpression ::selector} to the class, to create environment-aware file-outs:

{BasicClassOrganizer>>putCommentOnFile:numbered:moveSource:forClass: ::method}[embed]
') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyChangeSetSystemDictionary
^(EruditeBookSection basicNew title: 'SystemDictionary'; document: ((EruditeDocument contents: '!!!! Changes To SystemDictionary

Several methods in {SystemDictionary ::class} were changed to gather information not only
from the classes bound in [[[Smalltalk]]] but also from any environment in any environment implementation.

The methods changed are concerned with "class names" such as {SystemDictionary>>classNamed: ::method},
{SystemDictionary>>fillCaches ::method} or enumerating classes, such as {SystemDictionary>>allBehaviorsDo: ::method}.

[[[Smalltalk browseAllCallsOn: #environmentImplementationsIfPresentDo:]]] doItWithButton: The senders of #environmentImplementationsIfPresentDo:. give an overview of them. 
Some methods like {SystemDictionary>>at:put: ::method} use the environment-awareness of symbols to dispatch
the operation to the appropriate environment:

{SystemDictionary>>at:put: ::method}[embed].

The same is true for:

{SystemDictionary>>removeKey:ifAbsent: ::method}[embed].

Some other methods that need the same behaviour are defined in the [[[
browser _ SinglePackageBrowser new.
browser package: (CodePackage named: ''EnvironmentsBase'' createIfAbsent: false registerIfNew: false).
BrowserWindow open: browser label: browser labelString
]]] doItWithButton: EnvironmentsBase. package,
such as:

{SystemDictionary>>at: ::method}[embed]

or:

{SystemDictionary>>at:ifPresent:ifAbsent: ::method}[embed]') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyEnvironmentAwareness
^(EruditeBookSection basicNew title: 'EnvironmentAwareness'; document: ((EruditeDocument contents: '!!!! Environment Aware Symbols

The [[[
browser _ SinglePackageBrowser new.
browser package: (CodePackage named: ''EnvironmentAwareness'' createIfAbsent: false registerIfNew: false).
BrowserWindow open: browser label: browser labelString
]]] doItWithButton: EnvironmentAwareness. package provides environment aware symbols
and a {EnvironmentBindingIdentityDictionary ::class|subclass} of {IdentityDictionary ::class} to provide transparent handling of environment aware symbols.

Environment aware symbols are instances of a subclass of {AbstractSymbolInEnvironment ::class}.
For each new environment a new subclass of {AbstractSymbolInEnvironment ::class} is generated.
This is done by {AbstractSymbolInEnvironment>>subclassFor: ::method}. This way it is possible
to store the corresponding environment in a class instance variable.

The following code was copied from the [[[
browser _ SinglePackageBrowser new.
browser package: (CodePackage named: ''EnvironmentAwarenessTest'' createIfAbsent: false registerIfNew: false).
BrowserWindow open: browser label: browser labelString
]]] doItWithButton: EnvironmentAwarenessTest. package.

If these objects are given:

[[[
nowHashString _ DateAndTime now hash asString.
string1 _ ''S1'', nowHashString.
string2 _ ''S2'', nowHashString.
symbol1 _ string1 asSymbol.
symbol2 _ string2 asSymbol.	
environment1 _ Object new.
environment2 _ Object new.

symbol1in1 _ symbol1 forEnvironment: environment1.
symbol1in2 _ symbol1 forEnvironment: environment2.
symbol2in1 _ symbol2 forEnvironment: environment1.
symbol2in2 _ symbol2 forEnvironment: environment2]]] doIt

than these conditions concerning equality will hold:
[[[
self assert: symbol1 ~= symbol2.
self assert: symbol1 = symbol1in1.
self assert: symbol1 = symbol1in2.
self assert: symbol2 = symbol2in1.
self assert: symbol2 = symbol2in2.
self assert: symbol1in1 = symbol1in2.
self assert: symbol2in1 = symbol2in2.
self assert: symbol1in1 ~= symbol2in1.
self assert: symbol1in2 ~= symbol2in2]]] doIt

The following condtions concerning identity will hold:
[[[
self assert: symbol1 ~~ symbol2.
self assert: symbol1in1 ~~ symbol1in2.
self assert: symbol2in1 ~~ symbol2in2.
self assert: symbol1 ~~ symbol1in1.
self assert: symbol1 ~~ symbol1in2.
self assert: symbol2 ~~ symbol2in1.
self assert: symbol2 ~~ symbol2in2.
self assert: symbol1 == string1 asSymbol.
self assert: symbol2 == string2 asSymbol.]]] doIt

The environment aware symbols can tell their environment:
[[[
self assert: symbol1in1 environment == environment1.
self assert: symbol2in1 environment == environment1.
self assert: symbol1in2 environment == environment2.
self assert: symbol2in2 environment == environment2]]] doIt

The following conditions hold for the symbols classes:
[[[
self assert: symbol1 class == EnvironmentAwareSymbol.
self assert: symbol2 class == EnvironmentAwareSymbol.
self assert: string1 asSymbol class == EnvironmentAwareSymbol.
self assert: string2 asSymbol class == EnvironmentAwareSymbol.
self assert: (symbol1in1 isKindOf: AbstractSymbolInEnvironment).
self assert: (symbol1in2 isKindOf: AbstractSymbolInEnvironment).
self assert: (symbol2in1 isKindOf: AbstractSymbolInEnvironment).
self assert: (symbol2in2 isKindOf: AbstractSymbolInEnvironment).
self assert: symbol1in1 class == symbol2in1 class.
self assert: symbol1in2 class == symbol2in2 class.
self assert: symbol1in1 class ~~ symbol1in2 class.
self assert: symbol2in1 class ~~ symbol2in2 class.]]] doIt') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyEnvironmentsBase
^(EruditeBookSection basicNew title: 'EnvironmentsBase'; document: ((EruditeDocument contents: '!!!! Environments Base Package

The [[[
browser _ SinglePackageBrowser new.
browser package: (CodePackage named: ''EnvironmentsBase'' createIfAbsent: false registerIfNew: false).
BrowserWindow open: browser label: browser labelString
]]] doItWithButton: EnvironmentsBase. package provides the runtime system for environment
implementations and some abstract superclasses that mostly document the interfaces used by the runtime.') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new        add: self AnatomyEnvironmentsBaseImplemenationsRegistry;
        add: self AnatomyEnvironmentsBaseSystemOrganizer;
        add: self AnatomyEnvironmentsBaseEnvironmentManagers;
        add: self AnatomyEnvironmentsBaseClassBuilders;
        add: self AnatomyEnvironmentsBaseEnvironments;
 yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyEnvironmentsBaseClassBuilders
^(EruditeBookSection basicNew title: 'ClassBuilders'; document: ((EruditeDocument contents: '!!!!Class Builders

{AbstractEnvironmentClassBuilder ::class} contains the minimum functionality
that class buildes that deal with environments should provide.

Note that {AbstractEnvironmentClassBuilder>>configureNewMetaclass: ::method} deferrs
metaclass configuration to the metaclass:

{AbstractEnvironmentClassBuilder>>configureNewMetaclass: ::method}[embed]

Subclasses of {AbstractEnvironmentClassBuilder ::class} also need to provide
an implementation of {environmentFromSpecification: ::selector}. This
method is used to convert a symbol to an environment in:

{AbstractEnvironmentClassBuilder>>superclass:subclass:instanceVariableNames:classVariableNames:poolDictionaries:category:environment: ::method}[embed]') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyEnvironmentsBaseEnvironmentManagers
^(EruditeBookSection basicNew title: 'EnvironmentManagers'; document: ((EruditeDocument contents: '!!!!Environment Managers

{AbstractEnvironmentManager ::class} defines the interface that {EnvironmentImplementationsDictionary ::class}
expects from objects registered with it. The only exception is {scanClassDenotation: ::selector}
which is (mostly) used in [[[Smalltalk browseAllCallsOn: #scanClassDenotation:]]] doItWithButton: tools manageing change sets and packages..') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyEnvironmentsBaseEnvironments
^(EruditeBookSection basicNew title: 'Environments'; document: ((EruditeDocument contents: '!!!!AbstractEnvironment

{AbstractEnvironment ::class} defines the interface for environments.
This is partly the same interface as {AbstractEnvironmentManager ::class}describes,
because normaly the manager has to delegete these messages to its environments.

I addition to that it defines a subset of {IdentityDictionary ::class}''s methods,
needed for defining symbols and compiling.') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyEnvironmentsBaseImplemenationsRegistry
^(EruditeBookSection basicNew title: 'ImplemenationsRegistry'; document: ((EruditeDocument contents: '!!!!The Registry for Environment Implementations

There is a subclass of {IdentityDictionary ::class} called {EnvironmentImplementationsDictionary ::class}
that is used as the a registry for environment implementations. 
As [[[Smalltalk]]] this class has a single instance bound to [[[EnvironmentImplementations]]].
This is done by the class {initialize ::selector}-method.

Environment implementations should register a so called environment-manager using {at:put: ::selector}.') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
AnatomyEnvironmentsBaseSystemOrganizer
^(EruditeBookSection basicNew title: 'SystemOrganizer'; document: ((EruditeDocument contents: 'Environment Aware System Organizer

Cuis''s core''s {SystemOrganizer ::class} does not make a difference between class name
symbols, therefore an environment-aware {EnvironmentAwareSystemOrganizer ::class|subclass} was added that implements
{numberOfCategoryOfElement: ::selector} and {removeElement ::selector}in an environment-aware fashion.') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
Features
^(EruditeBookSection basicNew title: 'Features'; document: ((EruditeDocument contents: '!!!! Features

Environments provides means to let several environment implementations coexist in one image.
There is no built-in environment implementation, but an example is provided in the [[[
browser _ SinglePackageBrowser new.
browser package: (CodePackage named: ''SimpleEnvironments'' createIfAbsent: false registerIfNew: false).
BrowserWindow open: browser label: browser labelString
]]] doItWithButton: SimpleEnvironments. package.

Unlike Squeak''s environment implementation, this implementation uses the {SystemDictionary::class} 
to dispatch any message concerned with the manipulation of bindings to the right environment.
This has the big adavantage, that the rest of system does not need to know about the existence
of environments. (**TODO**: //There is a minor, but important exception to that rule//)') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
Introduction
^(EruditeBookSection basicNew title: 'Introduction'; document: ((EruditeDocument contents: '!!!! Environments in Cuis

by Gerald Klix (Gerald.Klix@klix.ch)

This document describes the support for environments in Cuis Smalltalk.') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
Requirements
^(EruditeBookSection basicNew title: 'Requirements'; document: ((EruditeDocument contents: '!!!! Requirements

This section describes the  author''s and Juan''s requirements for an environment implementation Cuis.


!!!!!! Gerald''s Requirements

The author''s requirements are the following:

//No Implicit Context//:
It should be possible to work with every environment without selecting a current environment. There should be synatx to explicitly access each and ever environments.

//No Additional UI Tools//:
It should be possible to specify the contents of an environment with the existing browsers.

//No Restrictions//:
Everything should work with environments, Browsers, ChangeSets, CodePackages, categories etc. pp.

//Orthogonality//:
There should be no built-in restriction, that binds an environment to a package or a category.

//Openness//:
I should be possible, to implement tools like environment browsers, to bind environments to categories or packages or
to implement concepts such as a current environment.


!!!!!! Juan''s Requirements

Juan had the following requirement:

"I want Cuis to be a good place for doing any kinds of experiments. Adding hooks for optional pluggable behavior is a big part of that."


!!!!!! Ken''s Requirements

Ken Dickey has the following goals for his PackageEnvironments:
"
Goals: 
- Multiple Classes with same name coexist in different package environments
- Unburden Smalltalk SystemDictionary by reducing the number of helper/support class names

Weak Goals: 
- Cuis users won''t notice until they need it
- Introduce fewest new concepts and mechanisms
- Work as expected with current tools
"') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
SimpleEnvironments
^(EruditeBookSection basicNew title: 'SimpleEnvironments'; document: ((EruditeDocument contents: '!!!!Simple Environments

The [[[
browser _ SinglePackageBrowser new.
browser package: (CodePackage named: ''SimpleEnvironments'' createIfAbsent: false registerIfNew: false).
BrowserWindow open: browser label: browser labelString
]]] doItWithButton: SimpleEnvironments. package provides a simple and hopefully understandable
implementation environment implementation.') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new        add: self SimpleEnvironmentsEnvironments;
        add: self SimpleEnvironmentsManager;
        add: self SimpleEnvironmentsClassBuilder;
        add: self SimpleEnvironmentsMetaclass;
 yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
SimpleEnvironmentsClassBuilder
^(EruditeBookSection basicNew title: 'ClassBuilder'; document: ((EruditeDocument contents: '!!!!Simple Environments ClassBuilder

{SimpleEnvironmentClassBuilder ::class} implements a very simple ClassBuilder for environments.
{SimpleEnvironmentClassBuilder>>environmentFromSpecification ::method} delegates
its task to {SimpleEnvironmentManager ::class}. {metaclassClass::selector} answers
{SimpleEnvironmentMetaclass ::class}, which is documented in the next section.
') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
SimpleEnvironmentsEnvironments
^(EruditeBookSection basicNew title: 'Environments'; document: ((EruditeDocument contents: '!!!!Simple Environments

{SimpleEnvironment ::class} provides very simple environments it uses an instance of
{EnvironmentBindingIdentityDictionary ::class} to handle binding environment aware symbols:

{SimpleEnvironment>>initialize ::method}[embed]

The subset of {IdentityDictionary ::class}''s interface necessary for environments is delegated
to this dictionary. By implementing {>> ::selector} it also provides a nice syntax to reference locally bound symbols.
') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
SimpleEnvironmentsManager
^(EruditeBookSection basicNew title: 'Manager'; document: ((EruditeDocument contents: '!!!!Simple Environments Manager

{SimpleEnvironmentManager ::class} implements the interface specified in {AbstractEnvironmentManager::class}.
It also implements {>> ::selector} tor provide a nice way to specify environments.
Its {printOn: ::selector} reflects this fact:

{SimpleEnvironment>>printOn: ::method}[embed]

{SimpleEnvironmentManager ::class}//''s class side protocol will be documented, when
Erudite supports class side method references.//') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
SimpleEnvironmentsMetaclass
^(EruditeBookSection basicNew title: 'Metaclass'; document: ((EruditeDocument contents: '!!!!Simple Environments Metaclass

{SimpleEnvironmentMetaclass ::class} implements a metaclass, that
is aware of {SimpleEnvironment ::class} instances. Non local bindings of
symbols are found using {SimpleEnvironmentMetaclass>>forNonLocalBindingEnvironmentsDo: ::method}.
{SimpleEnvironmentMetaclass>>configureFromClassBuilder: ::method} retrieves the
environment from the class builder. The various methods in the ''printing''-category
create a a string that evaluates to the class or the metaclass.
This method may be an illustrative example:

{SimpleEnvironmentMetaclass>>denotingExpressionForClass ::method}[embed]

{SimpleEnvironmentMetaclass>>printAdditionalDefinitionOn: ::method}
prints the part of the class definition, that is not printed by {ClassDescription>>definition ::method}:

{SimpleEnvironmentMetaclass>>printAdditionalDefinitionOn: ::method}[embed]') images: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!EnvironmentsDocumentation methodsFor: 'as yet unclassified' stamp: 'KLG 10/29/2020 18:24:31'!
initialize
    super initialize.
    title _ 'EnvironmentsDocumentation'.
        self addSection: self Introduction.
        self addSection: self Requirements.
        self addSection: self Features.
        self addSection: self Anatomy.
        self addSection: self SimpleEnvironments.
! !
